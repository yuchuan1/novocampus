PUT /_river/my_jdbc_river/_meta
{
    "type": "jdbc",
    "jdbc": {

        "driver": "com.mysql.jdbc.Driver",
        "url": "jdbc:mysql://localhost:3306/playcampus",
        "user": "root",
        "password": "!zaq123",

        "strategy": "simple",
        "poll": "1m",
        "versioning" : true,
        "autocommit" : true,
        "sql": "SELECT lesson_elements.id as _id, lesson_elements.author, lesson_elements.modified_by,    lesson_elements.grade_level,    lesson_elements.subject_id,    lesson_elements.title,    lesson_elements.body,    lesson_elements.cover_image,    lesson_elements.resource_url,    lesson_elements.you_tube_video_id,    lesson_elements.share,    lesson_elements.created_at,    lesson_elements.modified_at	FROM playcampus.lesson_elements;"
    },
    "index": {

        "index": "index_lesson_element",

        "type": "lesson_element",

        "bulk_size": 5000,
        "max_bulk_requests": 30      
    }
}