# Routes
# This file defines all application routes (Higher priority routes first)
# ~~~~

# Home page
GET     /                               controllers.Application.index
GET     /restricted                     controllers.Application.restricted
GET     /assets/javascript/routes.js    controllers.Application.jsRoutes

# Map static resources from the /public folder to the /assets URL path
GET     /assets/*file                   controllers.Assets.at(path="/public", file)

GET     /login                          controllers.Application.login
POST    /login                          controllers.Application.doLogin

GET     /logout                         com.feth.play.module.pa.controllers.Authenticate.logout
GET     /authenticate/:provider         com.feth.play.module.pa.controllers.Authenticate.authenticate(provider: String)

GET     /signup                         controllers.Application.signup
POST    /signup                         controllers.Application.doSignup

GET     /accounts/unverified            controllers.Signup.unverified
GET     /authenticate/:provider/denied  controllers.Signup.oAuthDenied(provider: String)

GET     /accounts/verify/:token         controllers.Signup.verify(token: String)
GET     /accounts/verify                controllers.Account.verifyEmail
GET     /accounts/exists                controllers.Signup.exists

GET     /accounts/password/reset/:token controllers.Signup.resetPassword(token: String)
POST    /accounts/password/reset        controllers.Signup.doResetPassword
POST    /accounts/password/change       controllers.Account.doChangePassword

GET     /accounts/add                   controllers.Account.link

GET     /accounts/link                  controllers.Account.askLink
POST    /accounts/link                  controllers.Account.doLink

GET     /accounts/merge                 controllers.Account.askMerge
POST    /accounts/merge                 controllers.Account.doMerge

GET     /login/password/forgot          controllers.Signup.forgotPassword(email: String ?= "")
POST    /login/password/forgot          controllers.Signup.doForgotPassword
GET     /accounts/password/change       controllers.Account.changePassword


# user
GET     /users/new/:part            controllers.UserController.create(part:String)
POST    /users/new/:part            controllers.UserController.save(part:String)
POST    /users/import               controllers.UserController.importUser()
#GET     /users/new                 controllers.UserController.create()

GET     /users/:id/edit             controllers.UserController.edit(id:String)
POST    /users/:id/edit             controllers.UserController.update(id:String)
GET     /users/:id/remove           controllers.UserController.remove(id:String)
POST    /users/:id/delete           controllers.UserController.delete(id:String)
GET     /users/pic/:id              controllers.UserController.picture(id:String)
GET     /users/:id/parentCode       controllers.UserController.generateParentCode(id:String)
GET     /users/:id                  controllers.UserController.view(id:String)
GET     /profile                    controllers.Application.profile
GET     /users                      controllers.UserController.list(p:Int ?= 0, s ?= "loginID", o ?= "asc", f ?= "")

# permission
GET     /permissions/new            controllers.PermissionController.create()
POST    /permissions                controllers.PermissionController.save()

# role
GET     /roles                      controllers.RoleController.list(p:Int ?= 0, s ?= "roleName", o ?= "asc", f ?= "")
GET     /roles/:id                  controllers.RoleController.edit(id:String)
POST    /roles/:id                  controllers.RoleController.update(id:String)


# school year
GET     /schoolYears/new            controllers.SchoolYearController.create()
POST    /schoolYears                controllers.SchoolYearController.save()
GET     /schoolYears/setCurrent     controllers.SchoolYearController.setCurrent()
POST    /schoolYears/setCurrent     controllers.SchoolYearController.saveCurrent()
GET     /schoolYears/:id            controllers.SchoolYearController.edit(id:String)
POST    /schoolYears/:id            controllers.SchoolYearController.update(id:String)
GET     /schoolYears/:id/remove     controllers.SchoolYearController.remove(id:String)
POST    /schoolYears/:id/delete     controllers.SchoolYearController.delete(id:String)
GET     /schoolYears                controllers.SchoolYearController.list()


# group
GET     /groups/new                     controllers.GroupController.create()
POST    /groups                         controllers.GroupController.save()
GET     /groups/:id/edit                controllers.GroupController.edit(id:String)
POST    /groups/:id/update              controllers.GroupController.update(id:String)
GET     /groups/:id/remove              controllers.GroupController.remove(id:String)
POST    /groups/:id/delete              controllers.GroupController.delete(id:String)

GET     /groups/:id/groupCode/remove    controllers.GroupController.removeGroupCode(id:String)
GET     /groups/:id/groupCode           controllers.GroupController.generateGroupCode(id:String)

GET     /groups/:id/addMember           controllers.GroupController.addMember(id:String)
POST    /groups/:id/saveMember          controllers.GroupController.saveMember(id:String)

GET     /groups/:id/searchStudent       controllers.GroupController.searchStudent(id:String)
POST    /groups/:id/searchUser          controllers.GroupController.searchUser(id:String)

GET     /groups/:id/deleteStudent       controllers.GroupController.deleteStudent(id:String)
POST    /groups/:id/deleteStudent       controllers.GroupController.deleteMember(id:String)
GET     /groups/:id                     controllers.GroupController.view(id:String)
GET     /groups                         controllers.GroupController.list(p:Int ?= 0, s ?= "name", o ?= "asc", f ?= "")

GET     /groupManagement                controllers.GroupManagement.list()


# announcement
GET     /announcements/new              controllers.AnnouncementController.create()
POST    /announcements                  controllers.AnnouncementController.save()
POST    /announcements/:id/delete       controllers.AnnouncementController.delete(id:String)
GET     /announcements/:id              controllers.AnnouncementController.edit(id:String)
POST    /announcements/:id              controllers.AnnouncementController.update(id:String)
GET     /announcements                  controllers.AnnouncementController.list(p:Int ?= 0, s ?= "title", o ?= "asc", f ?= "")

# course
GET     /courses                  controllers.CourseController.list(p:Int ?= 0, s ?= "title", o ?= "asc", f ?= "")
GET     /courses/new              controllers.CourseController.create()
POST    /courses                  controllers.CourseController.save()
GET     /courses/:id              controllers.CourseController.edit(id:String)
POST    /courses/:id              controllers.CourseController.update(id:String)
POST    /courses/:id/delete       controllers.CourseController.delete(id:String)

# course module
GET     /courseModules/new        controllers.CourseModuleController.create()
POST    /courseModules            controllers.CourseModuleController.save()
GET     /courseModules/:id              controllers.CourseModuleController.edit(id:String)
POST    /courseModules/:id              controllers.CourseModuleController.update(id:String)
GET     /courseModules                  controllers.CourseModuleController.list(p:Int ?= 0, s ?= "title", o ?= "asc", f ?= "")
POST    /courseModules/:id/delete       controllers.CourseModuleController.delete(id:String)

# subject
GET     /subjects/new        controllers.SubjectController.create()
POST    /subjects             controllers.SubjectController.save()
POST    /subjects/:id/delete       controllers.SubjectController.delete(id:String)
GET     /subjects/:id              controllers.SubjectController.edit(id:String)
POST    /subjects/:id              controllers.SubjectController.update(id:String)
GET     /subjects                  controllers.SubjectController.list(p:Int ?= 0, s ?= "name", o ?= "asc", f ?= "")

# course module
#GET     /courseModules/new            controllers.CourseModuleController.create()
#POST    /courseModules                   controllers.CourseModuleController.save()

# lesson
GET     /lessons/new              controllers.LessonController.create()
POST    /lessons                  controllers.LessonController.save()
GET     /lessons                  controllers.LessonController.list(p:Int ?= 0, s ?= "title", o ?= "asc", f ?= "")
GET     /lessons/:id              controllers.LessonController.edit(id:String)
POST    /lessons/:id              controllers.LessonController.update(id:String)
POST    /lessons/:id/delete       controllers.LessonController.delete(id:String)


# lesson element
GET     /lessonElements/new              controllers.LessonElementController.create()
POST    /lessonElements                  controllers.LessonElementController.save()
GET     /lessonElements                  controllers.LessonElementController.list(p:Int ?= 0, s ?= "title", o ?= "asc", f ?= "")


# Edit an existing lesson element
GET     /lessonElements/:id              controllers.LessonElementController.edit(id:String)
POST    /lessonElements/:id              controllers.LessonElementController.update(id:String)

# Delete a lesson element
POST    /lessonElements/:id/delete          controllers.LessonElementController.delete(id:String)

# Forum API
POST     /api/forum/new/:name                         controllers.ForumController.create(name:String)
POST	 /api/message/send				controllers.MessageController.send()
POST	 /api/message/get			controllers.MessageController.getMessages()


GET     /chatroom                     controllers.Application.chatRoom()
