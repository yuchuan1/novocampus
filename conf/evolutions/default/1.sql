# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table announcements (
  id                        varchar(255) not null,
  author                    varchar(255),
  modified_by               varchar(255),
  title                     varchar(255),
  body                      TEXT,
  media                     varchar(255),
  created_at                datetime not null,
  modified_at               datetime not null,
  constraint pk_announcements primary key (id))
;

create table assessment_items (
  id                        varchar(255) not null,
  author                    varchar(255),
  modified_by               varchar(255),
  name                      varchar(255),
  created_at                datetime not null,
  modified_at               datetime not null,
  constraint pk_assessment_items primary key (id))
;

create table cm_assessment_types (
  id                        varchar(255) not null,
  author                    varchar(255),
  modified_by               varchar(255),
  title                     varchar(255),
  weight                    integer,
  created_at                datetime not null,
  modified_at               datetime not null,
  constraint pk_cm_assessment_types primary key (id))
;

create table cm_assignments (
  id                        varchar(255) not null,
  author                    varchar(255),
  modified_by               varchar(255),
  title                     varchar(255),
  body                      TEXT,
  description               varchar(255),
  label                     varchar(255),
  created_at                datetime not null,
  modified_at               datetime not null,
  constraint pk_cm_assignments primary key (id))
;

create table courses (
  id                        varchar(255) not null,
  author                    varchar(255),
  modified_by               varchar(255),
  title                     varchar(255),
  body                      TEXT,
  enabled                   tinyint(1) default 0,
  cover_image_url           varchar(255),
  group_id                  varchar(255),
  course_module_id          varchar(255),
  created_at                datetime not null,
  modified_at               datetime not null,
  constraint uq_courses_title unique (title),
  constraint pk_courses primary key (id))
;

create table course_assessment_types (
  id                        varchar(255) not null,
  author                    varchar(255),
  modified_by               varchar(255),
  title                     varchar(255),
  weight                    integer,
  created_at                datetime not null,
  modified_at               datetime not null,
  constraint pk_course_assessment_types primary key (id))
;

create table course_assignments (
  id                        varchar(255) not null,
  author                    varchar(255),
  modified_by               varchar(255),
  title                     varchar(255),
  body                      TEXT,
  description               varchar(255),
  label                     varchar(255),
  created_at                datetime not null,
  modified_at               datetime not null,
  constraint pk_course_assignments primary key (id))
;

create table course_grades (
  id                        varchar(255) not null,
  author                    varchar(255),
  modified_by               varchar(255),
  title                     varchar(255),
  score                     float,
  instructor_comment        varchar(255),
  created_at                datetime not null,
  modified_at               datetime not null,
  constraint pk_course_grades primary key (id))
;

create table course_modules (
  id                        varchar(255) not null,
  author                    varchar(255),
  modified_by               varchar(255),
  title                     varchar(255),
  body                      TEXT,
  course_module_code        varchar(255),
  cover_image_url           varchar(255),
  shared                    tinyint(1) default 0,
  created_at                datetime not null,
  modified_at               datetime not null,
  constraint pk_course_modules primary key (id))
;

create table forums (
  id                        varchar(255) not null,
  name                      varchar(255),
  constraint pk_forums primary key (id))
;

create table grades (
  id                        varchar(255) not null,
  author                    varchar(255),
  modified_by               varchar(255),
  title                     varchar(255),
  score                     float,
  instructor_comment        varchar(255),
  created_at                datetime not null,
  modified_at               datetime not null,
  constraint pk_grades primary key (id))
;

create table groups (
  id                        varchar(255) not null,
  author                    varchar(255),
  modified_by               varchar(255),
  name                      varchar(255),
  short_name                varchar(255),
  grade_level               integer not null,
  school_year_id            varchar(255),
  room_teacher_id           varchar(255),
  group_code                varchar(255),
  forum_id                  varchar(255),
  created_at                datetime not null,
  modified_at               datetime not null,
  constraint ck_groups_grade_level check (grade_level in (0,1,2,3,4,5,6,7,8,9,10,11,12,13)),
  constraint pk_groups primary key (id))
;

create table lessons (
  id                        varchar(255) not null,
  author                    varchar(255),
  modified_by               varchar(255),
  grade_level               integer,
  title                     varchar(255),
  body                      TEXT,
  image_url                 varchar(255),
  share                     tinyint(1) default 0,
  created_at                datetime not null,
  modified_at               datetime not null,
  constraint ck_lessons_grade_level check (grade_level in (0,1,2,3,4,5,6,7,8,9,10,11,12,13)),
  constraint uq_lessons_title unique (title),
  constraint pk_lessons primary key (id))
;

create table lesson_elements (
  id                        varchar(255) not null,
  author                    varchar(255),
  modified_by               varchar(255),
  grade_level               integer,
  subject_id                varchar(255),
  title                     varchar(255),
  body                      TEXT,
  cover_image               varchar(255),
  resource_url              varchar(255),
  you_tube_video_id         varchar(255),
  share                     tinyint(1) default 0,
  created_at                datetime not null,
  modified_at               datetime not null,
  constraint ck_lesson_elements_grade_level check (grade_level in (0,1,2,3,4,5,6,7,8,9,10,11,12,13)),
  constraint uq_lesson_elements_title unique (title),
  constraint pk_lesson_elements primary key (id))
;

create table library_folders (
  id                        varchar(255) not null,
  author                    varchar(255),
  modified_by               varchar(255),
  name                      varchar(255),
  created_at                datetime not null,
  modified_at               datetime not null,
  constraint pk_library_folders primary key (id))
;

create table linked_account (
  id                        varchar(255) not null,
  author                    varchar(255),
  modified_by               varchar(255),
  user_id                   varchar(255),
  provider_user_id          varchar(255),
  provider_key              varchar(255),
  created_at                datetime not null,
  modified_at               datetime not null,
  constraint pk_linked_account primary key (id))
;

create table memberships (
  id                        varchar(255) not null,
  author                    varchar(255),
  modified_by               varchar(255),
  group_id                  varchar(255),
  user_id                   varchar(255),
  role                      integer,
  created_at                datetime not null,
  modified_at               datetime not null,
  constraint ck_memberships_role check (role in (0,1)),
  constraint pk_memberships primary key (id))
;

create table message (
  id                        varchar(255) not null,
  forum_id                  varchar(255),
  from_uid                  varchar(255),
  to_uid                    varchar(255),
  body                      varchar(255),
  sent                      datetime,
  constraint pk_message primary key (id))
;

create table school_years (
  id                        varchar(255) not null,
  author                    varchar(255),
  modified_by               varchar(255),
  academic_year             varchar(50) not null,
  semester                  integer not null,
  start_date                datetime,
  end_date                  datetime,
  description               varchar(255),
  created_at                datetime not null,
  modified_at               datetime not null,
  constraint ck_school_years_semester check (semester in (0,1,2,3)),
  constraint uq_school_years_1 unique (academic_year,semester),
  constraint pk_school_years primary key (id))
;

create table security_role (
  id                        varchar(255) not null,
  author                    varchar(255),
  modified_by               varchar(255),
  role_name                 varchar(255),
  created_at                datetime not null,
  modified_at               datetime not null,
  constraint pk_security_role primary key (id))
;

create table subjects (
  id                        varchar(255) not null,
  author                    varchar(255),
  modified_by               varchar(255),
  name                      varchar(255),
  created_at                datetime not null,
  modified_at               datetime not null,
  constraint pk_subjects primary key (id))
;

create table token_action (
  id                        varchar(255) not null,
  author                    varchar(255),
  modified_by               varchar(255),
  token                     varchar(255),
  target_user_id            varchar(255),
  type                      varchar(2),
  created                   datetime,
  expires                   datetime,
  created_at                datetime not null,
  modified_at               datetime not null,
  constraint ck_token_action_type check (type in ('EV','PR')),
  constraint uq_token_action_token unique (token),
  constraint pk_token_action primary key (id))
;

create table users (
  id                        varchar(255) not null,
  author                    varchar(255),
  modified_by               varchar(255),
  email                     varchar(64),
  email_validated           tinyint(1) default 0,
  login_id                  varchar(64),
  first_name                varchar(64),
  last_name                 varchar(64),
  gender                    integer,
  birth_date                datetime,
  active                    tinyint(1) default 0,
  last_login                datetime,
  picture                   varchar(2000),
  teacher_id                varchar(255),
  student_id                varchar(255),
  grade_level               integer,
  student_status            integer,
  parent_code               char(6),
  relationship              integer,
  created_at                datetime not null,
  modified_at               datetime not null,
  constraint ck_users_gender check (gender in (0,1)),
  constraint ck_users_grade_level check (grade_level in (0,1,2,3,4,5,6,7,8,9,10,11,12,13)),
  constraint ck_users_student_status check (student_status in (0,1,2,3)),
  constraint ck_users_relationship check (relationship in (0,1,2,3)),
  constraint pk_users primary key (id))
;

create table user_permission (
  id                        varchar(255) not null,
  author                    varchar(255),
  modified_by               varchar(255),
  value                     varchar(255),
  created_at                datetime not null,
  modified_at               datetime not null,
  constraint pk_user_permission primary key (id))
;

create table Variables (
  name                      varchar(50) not null,
  value                     varchar(255),
  constraint uq_Variables_name unique (name))
;


create table course_modules_lessons (
  course_modules_id              varchar(255) not null,
  lessons_id                     varchar(255) not null,
  constraint pk_course_modules_lessons primary key (course_modules_id, lessons_id))
;

create table lesson_elements_lessons (
  lesson_elements_id             varchar(255) not null,
  lessons_id                     varchar(255) not null,
  constraint pk_lesson_elements_lessons primary key (lesson_elements_id, lessons_id))
;

create table users_security_role (
  users_id                       varchar(255) not null,
  security_role_id               varchar(255) not null,
  constraint pk_users_security_role primary key (users_id, security_role_id))
;

create table permission_security_role (
  user_permission_id             varchar(255) not null,
  security_role_id               varchar(255) not null,
  constraint pk_permission_security_role primary key (user_permission_id, security_role_id))
;
alter table courses add constraint fk_courses_group_1 foreign key (group_id) references groups (id) on delete restrict on update restrict;
create index ix_courses_group_1 on courses (group_id);
alter table courses add constraint fk_courses_courseModule_2 foreign key (course_module_id) references course_modules (id) on delete restrict on update restrict;
create index ix_courses_courseModule_2 on courses (course_module_id);
alter table groups add constraint fk_groups_schoolYear_3 foreign key (school_year_id) references school_years (id) on delete restrict on update restrict;
create index ix_groups_schoolYear_3 on groups (school_year_id);
alter table groups add constraint fk_groups_roomTeacher_4 foreign key (room_teacher_id) references users (id) on delete restrict on update restrict;
create index ix_groups_roomTeacher_4 on groups (room_teacher_id);
alter table groups add constraint fk_groups_forum_5 foreign key (forum_id) references forums (id) on delete restrict on update restrict;
create index ix_groups_forum_5 on groups (forum_id);
alter table lesson_elements add constraint fk_lesson_elements_subject_6 foreign key (subject_id) references subjects (id) on delete restrict on update restrict;
create index ix_lesson_elements_subject_6 on lesson_elements (subject_id);
alter table linked_account add constraint fk_linked_account_user_7 foreign key (user_id) references users (id) on delete restrict on update restrict;
create index ix_linked_account_user_7 on linked_account (user_id);
alter table memberships add constraint fk_memberships_group_8 foreign key (group_id) references groups (id) on delete restrict on update restrict;
create index ix_memberships_group_8 on memberships (group_id);
alter table memberships add constraint fk_memberships_user_9 foreign key (user_id) references users (id) on delete restrict on update restrict;
create index ix_memberships_user_9 on memberships (user_id);
alter table message add constraint fk_message_forum_10 foreign key (forum_id) references forums (id) on delete restrict on update restrict;
create index ix_message_forum_10 on message (forum_id);
alter table token_action add constraint fk_token_action_targetUser_11 foreign key (target_user_id) references users (id) on delete restrict on update restrict;
create index ix_token_action_targetUser_11 on token_action (target_user_id);



alter table course_modules_lessons add constraint fk_course_modules_lessons_course_modules_01 foreign key (course_modules_id) references course_modules (id) on delete restrict on update restrict;

alter table course_modules_lessons add constraint fk_course_modules_lessons_lessons_02 foreign key (lessons_id) references lessons (id) on delete restrict on update restrict;

alter table lesson_elements_lessons add constraint fk_lesson_elements_lessons_lesson_elements_01 foreign key (lesson_elements_id) references lesson_elements (id) on delete restrict on update restrict;

alter table lesson_elements_lessons add constraint fk_lesson_elements_lessons_lessons_02 foreign key (lessons_id) references lessons (id) on delete restrict on update restrict;

alter table users_security_role add constraint fk_users_security_role_users_01 foreign key (users_id) references users (id) on delete restrict on update restrict;

alter table users_security_role add constraint fk_users_security_role_security_role_02 foreign key (security_role_id) references security_role (id) on delete restrict on update restrict;

alter table permission_security_role add constraint fk_permission_security_role_user_permission_01 foreign key (user_permission_id) references user_permission (id) on delete restrict on update restrict;

alter table permission_security_role add constraint fk_permission_security_role_security_role_02 foreign key (security_role_id) references security_role (id) on delete restrict on update restrict;

# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table announcements;

drop table assessment_items;

drop table cm_assessment_types;

drop table cm_assignments;

drop table courses;

drop table course_assessment_types;

drop table course_assignments;

drop table course_grades;

drop table course_modules;

drop table course_modules_lessons;

drop table forums;

drop table grades;

drop table groups;

drop table lessons;

drop table lesson_elements_lessons;

drop table lesson_elements;

drop table library_folders;

drop table linked_account;

drop table memberships;

drop table message;

drop table school_years;

drop table security_role;

drop table users_security_role;

drop table permission_security_role;

drop table subjects;

drop table token_action;

drop table users;

drop table user_permission;

drop table Variables;

SET FOREIGN_KEY_CHECKS=1;

