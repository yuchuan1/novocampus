package utils;

import java.util.LinkedHashMap;
import java.util.Map;

public class BreadCrumb {
	
	public Map<String, String> breadCrumbs = new LinkedHashMap<String, String>();
	public String activeCrumb;
	public String activeCrumbUrl;
	public String nav;
	public String destination;

	public BreadCrumb(String activeCrumb) {
		super();
		this.activeCrumb = activeCrumb;
		this.activeCrumbUrl = "";
	}

  public BreadCrumb(String activeCrumb, String url) {
    super();
    this.activeCrumb = activeCrumb;
    this.activeCrumbUrl = url;
  }

	public void Add(String title, String url)
	{				
		breadCrumbs.put(title, url);
	}

}
