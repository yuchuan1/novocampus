package utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VideoHelper {

	public static String getYouTubeVideoID(String youtubeVideoUrl)
	{
		String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
		Pattern compiledPattern = Pattern.compile(pattern);
		Matcher matcher = compiledPattern.matcher(youtubeVideoUrl);
		if(matcher.find()){
	        return matcher.group();
	    }
		else
			return null;
	}
	
	public static boolean resourceIsYouTubeVideo(String url)
	{
		return url.toLowerCase().contains("youtube.com");
	}
}
