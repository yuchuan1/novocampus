package utils;

public class PageHelper {
	
  public String currentModel;
	public String currentSortBy;
  public String currentFilter;
	public String currentOrder;

	// from com.avaje.ebean.Page<T>
	public int pageIndex;
  public int totalPageCount;
  public int totalRowCount;
  public boolean hasNext;
  public boolean hasPrev;

  public PageHelper(String useModel, String sortBy, String order, String filter, int pageIndex, int totalPageCount, int totalRowCount, boolean hasPrev, boolean hasNext) {
    this.currentModel = useModel;
    this.currentSortBy = sortBy;
    this.currentOrder = order;
    this.currentFilter = filter;
    this.pageIndex = pageIndex;
    this.totalPageCount = totalPageCount;
    this.totalRowCount = totalRowCount;
    this.hasPrev = hasPrev;
    this.hasNext = hasNext;
  }
}
