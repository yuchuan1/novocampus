package views.formdata;

import java.util.ArrayList;
import java.util.List;
import play.data.validation.Constraints;
import models.enums.GradeLevel;

public class LessonFormData {
	
	public String id;
	

	public GradeLevel gradeLevel;
	

	public String subject;
	
	@Constraints.Required
	public String title;
	
	@Constraints.MaxLength(4096)
	public String body;
	
	@Constraints.MaxLength(2000)
	public String image_url;
	public Boolean share;
	
	public List<String> lessonElements = new ArrayList<>();
}
