package views.formdata;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import play.data.format.Formats;
import play.data.validation.Constraints;
import models.SecurityRole;
import models.User;
import models.enums.Gender;
import models.enums.GradeLevel;
import models.enums.Relationship;
import models.enums.StudentStatus;

/**
 * Backing class for the User data form.
 * Requirements:
 * <ul>
 * <li> All fields are public, 
 * <li> A public no-arg constructor.
 * <li> A validate() method that returns null or a List[ValidationError].
 * </ul>
 */

public class UserFormData {
	public String id;
	
	@Constraints.MaxLength(64)
	public String email = "";
	
	@Constraints.MaxLength(64)
	public String loginID = "";
	
	@Constraints.MaxLength(64)
	public String firstName = "";
	
	@Constraints.MaxLength(64)
	public String lastName = "";
	public boolean active;
	
	@Constraints.Required
	public Gender gender;
	
	@Constraints.MaxLength(2000)
	public String picture = "";
	public List<String> roles = new ArrayList<>();
	
	@Formats.DateTime(pattern = "yyyy-MM-dd")
	public Date birthDate;
	
	// student properties
	public String studentId = "";
	public GradeLevel gradeLevel = GradeLevel.Ten;
	public StudentStatus studentStatus = StudentStatus.Active;
	
	// teacher properties
		public String teacherID;
		
		// parent properties
		public Relationship relationship;
	
	public UserFormData() {
		super();	

	}
	
	public UserFormData(String id) {
		super();	
		
		User user = User.find.byId(id);
		this.email = user.email;
		this.loginID = user.loginID;
		this.id = user.id;
		this.active = user.active;
		this.firstName = user.firstName;
		this.gender = user.gender;
		this.lastName = user.lastName;
		this.picture = user.picture;
		this.studentId = user.studentID;
		this.gradeLevel = user.gradeLevel;
		this.studentStatus = user.studentStatus;
		this.teacherID = user.teacherID;
		this.relationship = user.relationship;
		
		
		for(SecurityRole role : user.roles)
		{
			this.roles.add(role.roleName);
		}

	}
}
