package views.formdata;

import play.data.validation.Constraints.*;
import models.enums.GradeLevel;

public class GroupFormData {
  
  public String id;
  
  @Required
  public String name;
  
  @MaxLength(50)
  public String shortName;
  
  @Required
  public GradeLevel gradeLevel;
  
  public String schoolYear;
  public String roomTeacher;
}