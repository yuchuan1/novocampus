package views.formdata;

import models.CourseModule;
import play.data.validation.Constraints.*;

public class CourseFormData {
	
	public String id;
	
	@Required
	@MaxLength(50)
	public String title;
	
	@MaxLength(4096)
	public String body;
	
	public Boolean enabled;
	public String coverImageUrl;
	public String group;
	public CourseModule courseModule;
}
