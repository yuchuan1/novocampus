package views.formdata;

import java.util.ArrayList;
import java.util.List;

import models.SecurityRole;
import models.UserPermission;

public class RoleFormData {
	public String roleName;
	public String id;
	public List<String> permissions = new ArrayList<>();
	
	public RoleFormData() {
		super();
	}
	
	public RoleFormData(String id) {
		super();
		SecurityRole role = SecurityRole.find.byId(id);
		this.roleName = role.roleName;
		for(UserPermission permission : role.permissions)
		{
			this.permissions.add(permission.value);
		}
	}
}
