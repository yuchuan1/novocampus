package views.formdata;

import java.util.ArrayList;
import java.util.List;

import play.data.validation.Constraints;

public class CourseModuleFormData {

	public String id;
	
	@Constraints.MaxLength(50)
	public String title;
	
	@Constraints.MaxLength(4096)
	public String body;
	
	@Constraints.MaxLength(50)
	public String courseModuleCode;
	public String coverImageUrl;
	public Boolean shared;
	
	public List<String> lessons = new ArrayList<>();
}
