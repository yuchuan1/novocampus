package views.formdata;

import play.data.validation.Constraints.Required;

import java.util.ArrayList;
import java.util.List;

public class GroupAddMemberForm {

  @Required
  public String id;
  
  @Required
  public List<String> uids = new ArrayList<>();

}