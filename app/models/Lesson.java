package models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import models.enums.GradeLevel;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Page;
import com.google.common.base.Strings;

import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;
import views.formdata.CourseModuleFormData;
import views.formdata.LessonFormData;


@Entity
@Table(name = "lessons")
public class Lesson extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	
	public Lesson() {
		super();
		this.id = UUID.randomUUID() + "";
		this.lessonElements = new ArrayList<>();
	}

	@Id
	public String id;
	
	@Constraints.Required
	public GradeLevel gradeLevel;
	
	@Constraints.Required
	public Subject subject;
	
	@Constraints.Required
	@Column(unique=true)
	public String title;
	
	@Constraints.MaxLength(4096)
	@Column(columnDefinition="TEXT")
	public String body;
	public String image_url;
	
	public Boolean share = true;
				
	@ManyToMany(mappedBy = "lessons", cascade = CascadeType.ALL)
	public List<LessonElement> lessonElements = new ArrayList<>();
	
	@ManyToMany(mappedBy = "lessons", cascade = CascadeType.ALL)
	public List<CourseModule> courseModules =  new ArrayList<>();
	
	/**
     * Generic query helper for entity Lesson with id String
     */
	public static final Finder<String, Lesson> find = new Finder<String, Lesson>(
			String.class, Lesson.class);
	
	/**
     * Return a page of lessons
     *
     * @param page Page to display
     * @param pageSize Number of LessonElements per page
     * @param sortBy Lesson property used for sorting
     * @param order Sort order (either or asc or desc)
     * @param filter Filter applied on the title column
     */
    public static Page<Lesson> page(int page, int pageSize, String sortBy, String order, String filter) {
        return 
            find.where()
                .ilike("title", "%" + filter + "%")
                .orderBy(sortBy + " " + order)                
                .findPagingList(pageSize)
                .setFetchAhead(false)
                .getPage(page);
    }
    
    /**
     * Return an LessonFormData instance constructed from a lesson instance.
     * @param id The ID of a lesson instance.
     * @return The LessonFormData instance, or throws a RuntimeException. 
     */
    public static LessonFormData makeLessonFormData(String id) {
    	try
    	{    	
    		Lesson lesson = Lesson.find.byId(id);
    		LessonFormData formData = new LessonFormData();
    		
    		formData.body = lesson.body;
    		formData.gradeLevel = lesson.gradeLevel;
    		formData.id = lesson.id;
    		formData.image_url = lesson.image_url;
    		formData.share = lesson.share;
    		
    		formData.title = lesson.title;
    		for (LessonElement element : lesson.lessonElements) {
    			formData.lessonElements.add(lesson.title);	    	        
	    	}
	    	
	    	return formData;      
    	}
    	catch( Exception ex)
    	{
    		throw new RuntimeException("Couldn't find lesson");
    	}
    }
    
    /**
     * Returns a lesson instance created from the form data.
     * Assumes that the formData has been validated.
     * The ID field is not assigned or managed in this application.
     * @param formData The lesson form data.
     * @return An user instance. 
     */
    public static Lesson makeInstance(LessonFormData formData) {
    	Lesson lesson = Lesson.find.byId(formData.id);
    	lesson.body = formData.body;
    	lesson.gradeLevel = formData.gradeLevel;
    	lesson.image_url = formData.image_url;
    	lesson.share = formData.share;
    	if(!Strings.isNullOrEmpty(formData.subject))
    		lesson.subject = Subject.find.byId(formData.subject);
    	lesson.title = formData.title; 
    	
    	Ebean.deleteManyToManyAssociations(lesson,"lessonElements");
    	
      for (String lessonElement : formData.lessonElements) {
    	  lesson.lessonElements.add(LessonElement.find.where().eq("title", lessonElement).findUnique() );
      }
      
      lesson.saveManyToManyAssociations("lessonElements");

      return lesson;
    }
    
    public static void removeLessonElement(String lessonId, String elementId)
    {
    	Lesson lesson = Lesson.find.byId(lessonId);
    	lesson.lessonElements.remove(LessonElement.find.ref(String.valueOf(elementId)));
    	lesson.saveManyToManyAssociations("lessonElements");
    }
    
    public static Map<String, Boolean> makeLessonMap() {
	    Map<String, Boolean> lessonMap = new HashMap<String, Boolean>();
	    for (Lesson lesson : Lesson.find.findList()) {	    	
	    	lessonMap.put(lesson.title, false);
	    }
	    return lessonMap;
	  } 

    
    public static Map<String, Boolean> makeLessonMap(CourseModuleFormData courseModuleFormData) {
	    Map<String, Boolean> lessonMap = new HashMap<String, Boolean>();
	    for (Lesson lesson : Lesson.find.findList()) {	    	
	    	lessonMap.put(lesson.title, (lesson != null && courseModuleFormData.lessons.contains(lesson.title)));
	    }
	    return lessonMap;
	  }

}
