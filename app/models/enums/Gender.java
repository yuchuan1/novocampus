package models.enums;

public enum Gender {
  Female(0),
  Male(1);
    
  public final int id;
	
  Gender(int id){
		this.id = id;
	}
	
}