package models.enums;

public enum MembershipRole {
	Admin(0),
	Member(1);

	public final int id;
  
	MembershipRole(int id){
    this.id = id;
  }
  
}
