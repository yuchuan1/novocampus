package models.enums;

public enum Relationship {
	Father, Mother, Guardian, Other
}
