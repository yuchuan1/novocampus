package models.enums;

public enum GradeLevel {
	Kindergarten(0),
	One(1),
	Two(2),
	Three(3),
	Four(4),
	Five(5),
	Six(6),
	Seven(7),
	Eight(8),
	Nine(9),
	Ten(10),
	Eleven(11),
	Twelve(12),
	Others(13);
	
	public final int id;
  
	GradeLevel(int id){
    this.id = id;
  }
  
}
