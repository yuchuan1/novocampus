package models.enums;

public enum Semester {
	One(1),
	Two(2),
	Three(3),
	Four(4);
	
	public final int id;
  
	Semester(int id){
    this.id = id;
  }

}
