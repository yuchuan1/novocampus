package models.enums;

public enum StudentStatus {
	Active, DroppedOut, Graduated, Suspended
}
