package models;

import java.util.List;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
@Table(name = "library_folders")
public class LibraryFolder extends BaseModel {

	private static final long serialVersionUID = 1L;
	
	@Id
	public String id;
	@Constraints.MaxLength(50)
	public String name;
	public List<String> fileUrls;
	
	public LibraryFolder() {
		super();
		this.id = UUID.randomUUID() + "";
	} 
	
	/**
     * Generic query helper for entity LibraryFolder with id String
     */
	public static final Finder<String, LibraryFolder> find = new Finder<String, LibraryFolder>(
			String.class, LibraryFolder.class);
}
