package models;

import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
@Table(name = "course_assignments")
public class CourseAssignment extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	public String id;
	
	@Constraints.Required
	@Constraints.MaxLength(50)
	public String title;
	@Constraints.MaxLength(4096)
	@Column(columnDefinition="TEXT")
	public String body;
	@Constraints.MaxLength(50)
	public String description;
	@Constraints.MaxLength(50)
	public String label;
	public CourseModule courseModule;
	public CMAssessmentType assessmentType;
	public List<String> uploadedFiles;

	public CourseAssignment() {
		super();
		this.id = UUID.randomUUID() + "";
	}
	
	/**
     * Generic query helper for entity CourseAssignment with id String
     */
	public static final Finder<String, CourseAssignment> find = new Finder<String, CourseAssignment>(
			String.class, CourseAssignment.class);
}
