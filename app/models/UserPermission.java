package models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import play.data.validation.Constraints;
import play.db.ebean.Model;
import views.formdata.RoleFormData;
import be.objectify.deadbolt.core.models.Permission;

/**
 * Initial version based on work by Steve Chaloner (steve@objectify.be) for
 * Deadbolt2
 */
@Entity
public class UserPermission extends BaseModel implements Permission {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	public String id;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name="permission_security_role")
    public List<SecurityRole> roles;

	public UserPermission() {
		super();
		this.id = UUID.randomUUID() + "";
	}

	@Constraints.MaxLength(50)
	public String value;

	public static final Model.Finder<String, UserPermission> find = new Model.Finder<String, UserPermission>(
			String.class, UserPermission.class);

	public String getValue() {
		return value;
	}

	public static UserPermission findByValue(String value) {
		return find.where().eq("value", value).findUnique();
	}
	
	 /**
	   * Create a map of permission name -> boolean including all known permission
	   * and setting the boolean to true if a given permission is associated with the passed role.
	   * @param role A role which may have zero or more permissions, or null to create a permission list
	   * with all unchecked boxes.
	   * @return A map of permission names to booleans indicating the permissions associated with the role.
	   */
	  public static Map<String, Boolean> makePermissionMap(RoleFormData role) {
	    Map<String, Boolean> permissionMap = new HashMap<String, Boolean>();
	    for (UserPermission permission : UserPermission.find.all()) {
	    	permissionMap.put(permission.value, (permission != null && role.permissions.contains(permission.value)));
	    }
	    return permissionMap;
	  }
	  
	// default checkbox settings for creating new role
	  public static Map<String, Boolean> makePermissionMap() {
		  Map<String, Boolean> permissionMap = new HashMap<String, Boolean>();
		  for (UserPermission permission : UserPermission.find.all()) {
		    	permissionMap.put(permission.value,false);
		    }
		    return permissionMap;
	  }
}
