package models;

import javax.persistence.Column;
import javax.persistence.Id;

import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

public class GradeQuiz extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	public String id;
	@Constraints.Required
	@Constraints.MaxLength(50)
	public String title;
	@Constraints.MaxLength(4096)
	@Column(columnDefinition="TEXT")
	public String body;
	
	/**
     * Generic query helper for entity GradeQuiz with id String
     */
	public static final Finder<String, GradeQuiz> find = new Finder<String, GradeQuiz>(
			String.class, GradeQuiz.class);
	
}
