package models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Page;

import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

import views.formdata.CourseModuleFormData;

@Entity
@Table(name = "course_modules")
public class CourseModule extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	public String id;
	
	public CourseModule() {
		super();
		this.id = UUID.randomUUID() + "";
	}
	
	@Constraints.Required
	@Constraints.MaxLength(50)
	public String title;
	@Constraints.MaxLength(4096)
	@Column(columnDefinition="TEXT")
	public String body;
	@Constraints.MaxLength(50)
	public String courseModuleCode;
	public String coverImageUrl;
	public Boolean shared;
	
	@ManyToMany
	public List<Lesson> lessons = new ArrayList<Lesson>();
	
	// This means one course module can have many courses associated
    @OneToMany(mappedBy = "courseModule")
	public List<Course> courses = new ArrayList<Course>();
	
	/**
     * Generic query helper for entity CourseModule with id String
     */
	public static final Finder<String, CourseModule> find = new Finder<String, CourseModule>(
			String.class, CourseModule.class);
	
	/**
     * Return a page of CourseModule
     *
     * @param page Page to display
     * @param pageSize Number of Course Modules per page
     * @param sortBy CourseModule property used for sorting
     * @param order Sort order (either or asc or desc)
     * @param filter Filter applied on the title column
     */
    public static Page<CourseModule> page(int page, int pageSize, String sortBy, String order, String filter) {
        return 
            find.where()
                .ilike("title", "%" + filter + "%")
                .orderBy(sortBy + " " + order)                
                .findPagingList(pageSize)
                .setFetchAhead(false)
                .getPage(page);
    }
    
    /**
     * Return an CourseModuleFormData instance constructed from a course module instance.
     * @param id The ID of a course module instance.
     * @return The CourseModuleFormData instance, or throws a RuntimeException. 
     */
    public static CourseModuleFormData makeCourseModuleFormData(String id) {
    	try
    	{    
    		CourseModule module = CourseModule.find.byId(id);
    		CourseModuleFormData formData = new CourseModuleFormData();
    		
    		formData.body = module.body;
    		formData.courseModuleCode = module.courseModuleCode;
    		formData.coverImageUrl = module.coverImageUrl;
    		formData.id = module.id;
    		formData.shared = module.shared;
    		formData.title = module.title;
    		    		
    		for (Lesson lesson : module.lessons) {
    			formData.lessons.add(lesson.title);	    	        
	    	}
	    	
	    	return formData;      
    	}
    	catch( Exception ex)
    	{
    		throw new RuntimeException("Couldn't find course module");
    	}
    }
    

    
    /**
     * Returns a course module instance created from the form data.
     * Assumes that the formData has been validated.
     * The ID field is not assigned or managed in this application.
     * @param formData The course module form data.
     * @return A course module instance. 
     */
    public static CourseModule makeInstance(CourseModuleFormData formData) {
    	CourseModule module = CourseModule.find.byId(formData.id);
    	module.body = formData.body;
    	module.courseModuleCode = formData.courseModuleCode;
    	module.coverImageUrl = formData.coverImageUrl;
    	module.shared = formData.shared;
    	module.title = formData.title;
    	Ebean.deleteManyToManyAssociations(module,"lessons");
    	
      for (String lesson : formData.lessons) {
    	  module.lessons.add(Lesson.find.where().eq("title", lesson).findUnique() );
      }
      
      module.saveManyToManyAssociations("lessons");

      return module;
    }
    
    public static Map<String,String> options() {
        LinkedHashMap<String,String> options = new LinkedHashMap<String,String>();
        for(CourseModule c: CourseModule.find.orderBy("title").findList()) {
            options.put(c.id.toString(), c.title);
        }
        return options;
    }
    
    public static Map<String, Boolean> makeCourseModuleMap() {
	    Map<String, Boolean> courseModuleMap = new HashMap<String, Boolean>();
	    for (CourseModule module : CourseModule.find.findList()) {	    	
	    	courseModuleMap.put(module.title, false);
	    }
	    return courseModuleMap;
	  }
}
