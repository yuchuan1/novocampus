package models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.UpdatedTimestamp;

import controllers.Application;
import play.db.ebean.Model;

@MappedSuperclass
public class BaseModel extends Model{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name="author")
	public String author;
	
	@Column(name="created_at")
	@CreatedTimestamp
	public Date created_at;
	
	@Column(name="modified_by")
	public String modifiedBy;
	
	@Column(name="modified_at")
	@Version
	@UpdatedTimestamp
	public Date modified_at;
	
	public void save() {
        User logged = Application.getLocalUser();
        if (logged != null) {
            this.author = logged.id;
            this.modifiedBy = logged.id;
        }
        super.save();
    }
	
	public void update(Object o) {
	    User logged = Application.getLocalUser();
	    if (logged != null) {
	        this.modifiedBy = logged.id;
	    }
	    super.update(o);
	}

}
