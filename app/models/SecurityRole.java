/*
 * Copyright 2012 Steve Chaloner
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Page;

import controllers.Application;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import views.formdata.RoleFormData;
import views.formdata.UserFormData;
import be.objectify.deadbolt.core.models.Role;

/**
 * @author Steve Chaloner (steve@objectify.be)
 */
@Entity
public class SecurityRole extends BaseModel implements Role {
  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;

  @Id
  public String id;

  public SecurityRole() {
    super();
    this.id = UUID.randomUUID() + "";
  }

  @Constraints.MaxLength(50)
  public String roleName;

  @ManyToMany(mappedBy = "roles")
  public List<User> users;

  @ManyToMany(mappedBy = "roles", cascade = CascadeType.ALL)
  public List<UserPermission> permissions;

  public static final Finder<String, SecurityRole> find = new Finder<String, SecurityRole>(String.class, SecurityRole.class);

  @Override
  public String getName() {
    return roleName;
  }

  public static SecurityRole findByRoleName(String roleName) {
    return find.where().eq("roleName", roleName).findUnique();
  }

  public static Map<String, String> options() {
    LinkedHashMap<String, String> options = new LinkedHashMap<String, String>();
    for (SecurityRole c : SecurityRole.find.orderBy("roleName").findList()) {
      if (!c.roleName.equals(Application.USER_ROLE))
        options.put(c.id.toString(), c.roleName);
    }
    return options;
  }

  /**
   * Create a map of role name -> boolean including all known role and setting
   * the boolean to true if a given role is associated with the passed user.
   * 
   * @param user
   *          An user who may have zero or more roles, or null to create a role
   *          list with all unchecked boxes.
   * @return A map of role names to booleans indicating the roles associated
   *         with the user.
   */
  public static Map<String, Boolean> makeRoleMap(User user) {
    Map<String, Boolean> roleMap = new HashMap<String, Boolean>();
    for (SecurityRole role : SecurityRole.find.orderBy("roleName").findList()) {
      roleMap.put(role.getName(), (user != null && user.roles.contains(role.getName())));
    }
    return roleMap;
  }

  // default checkbox settings for creating new user
  public static Map<String, Boolean> makeRoleMap() {
    Map<String, Boolean> roleMap = new HashMap<String, Boolean>();
    for (SecurityRole role : SecurityRole.find.orderBy("roleName").findList()) {
      if (!role.roleName.equals(Application.USER_ROLE))
        roleMap.put(role.getName(), false);
    }
    return roleMap;
  }

  public static Map<String, Boolean> makeRoleMap(String id) {
    User user = User.find.where().eq("id", id).findUnique();

    Map<String, Boolean> roleMap = new HashMap<String, Boolean>();
    for (SecurityRole role : SecurityRole.list()) {
      if (!role.roleName.equals(Application.USER_ROLE))
        roleMap.put(role.getName(), (user != null && user.roles.contains(role.getName())));
    }
    return roleMap;
  }

  public static Map<String, Boolean> makeRoleMap(UserFormData user) {
    Map<String, Boolean> roleMap = new HashMap<String, Boolean>();
    for (SecurityRole role : SecurityRole.list()) {
      //if (!role.roleName.equals(Application.USER_ROLE))
        roleMap.put(role.roleName, (role != null && user.roles.contains(role.roleName)));
    }
    return roleMap;
  }

  public static List<SecurityRole> list() {
    List<SecurityRole> result = new ArrayList<>();
    for (SecurityRole c : SecurityRole.find.orderBy("roleName").findList()) {
      result.add(c);
    }

    return result;
  }

  /**
   * Return a RoleFormData instance constructed from a role instance.
   * 
   * @param id
   *          The ID of a role instance.
   * @return The RoleFormData instance, or throws a RuntimeException.
   */
  public static RoleFormData makeRoleFormData(String id) {
    try {
      SecurityRole role = SecurityRole.find.byId(id);
      RoleFormData formData = new RoleFormData();
      formData.roleName = role.roleName;
      formData.id = role.id;

      for (UserPermission permission : role.permissions) {
        formData.permissions.add(permission.value);
      }

      return formData;
    } catch (Exception ex) {
      throw new RuntimeException("Couldn't find role");
    }
  }

  /**
   * Returns a role instance created from the form data. Assumes that the
   * formData has been validated. The ID field is not assigned or managed in
   * this application.
   * 
   * @param formData
   *          The role form data.
   * @return A role instance.
   */
  public static SecurityRole makeInstance(RoleFormData formData) {
    SecurityRole role = SecurityRole.find.byId(formData.id);
    Ebean.deleteManyToManyAssociations(role, "permissions");

    for (String permission : formData.permissions) {
      role.permissions.add(UserPermission.findByValue(permission));
    }

    role.saveManyToManyAssociations("permissions");

    return role;
  }

  public static void removePermission(String roleId, String permissionId) {
    SecurityRole role = SecurityRole.find.byId(roleId);
    role.permissions.remove(UserPermission.find.ref(String.valueOf(permissionId)));
    role.saveManyToManyAssociations("permissions");
  }

  /**
   * Return a page of role
   * 
   * @param page
   *          Page to display
   * @param pageSize
   *          Number of Products per page
   * @param sortBy
   *          Role property used for sorting
   * @param order
   *          Sort order (either or asc or desc)
   * @param filter
   *          Filter applied on the name column
   */
  public static Page<SecurityRole> page(int page, int pageSize, String sortBy, String order, String filter) {
    return find.where().ilike("roleName", "%" + filter + "%").orderBy(sortBy + " " + order).findPagingList(pageSize).setFetchAhead(false).getPage(page);
  }

  public static SecurityRole getStudentRole() {
    return findByRoleName(Application.STUDENT_ROLE);
  }

}
