package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
@Table(name = "cm_assessment_types")
public class CMAssessmentType  extends BaseModel{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	public String id;
	
	@Constraints.Required
	@Constraints.MaxLength(50)
	public String title;
	public int weight;
	
	/**
     * Generic query helper for entity CMAssessmentType with id String
     */
	public static final Finder<String, CMAssessmentType> find = new Finder<String, CMAssessmentType>(
			String.class, CMAssessmentType.class);
}
