package models;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.avaje.ebean.Page;

import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
@Table(name = "subjects")
public class Subject extends BaseModel{

private static final long serialVersionUID = 1L;
	
	@Id
	public String id;
	
	@Constraints.Required
	@Constraints.MaxLength(50)
	public String name;
	
	// This means one subject can have many lesson element associated
  @OneToMany(mappedBy = "subject")
	public List<LessonElement> lessonElements; 

	public Subject() {
		super();
		this.id = UUID.randomUUID() + "";
	}
	
	/**
     * Generic query helper for entity Subject with id String
     */
	public static final Finder<String, Subject> find = new Finder<String, Subject>(
			String.class, Subject.class);
	
	/**
     * Return a page of subjects
     *
     * @param page Page to display
     * @param pageSize Number of subjects per page
     * @param sortBy subject property used for sorting
     * @param order Sort order (either or asc or desc)
     * @param filter Filter applied on the title column
     */
    public static Page<Subject> page(int page, int pageSize, String sortBy, String order, String filter) {
        return 
            find.where()
                .ilike("name", "%" + filter + "%")
                .orderBy(sortBy + " " + order)                
                .findPagingList(pageSize)
                .setFetchAhead(false)
                .getPage(page);
    }
    
    public static Map<String,String> options() {
        LinkedHashMap<String,String> options = new LinkedHashMap<String,String>();
        for(Subject c: Subject.find.orderBy("name").findList()) {
            options.put(c.id.toString(), c.name);
        }
        return options;
    }
}
