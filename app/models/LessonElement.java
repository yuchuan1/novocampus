package models;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.*;

import org.hibernate.validator.constraints.URL;

import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

import models.enums.GradeLevel;
import play.data.validation.*;
import views.formdata.LessonFormData;

import com.avaje.ebean.*;

@Entity
@Table(name = "lesson_elements")
public class LessonElement  extends BaseModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LessonElement() {
		super();
		this.id = UUID.randomUUID() + "";		
	}

	@Id
	public String id;
	
	public GradeLevel gradeLevel;
	
	// This means every lesson element has exactly one subject associated 
	@ManyToOne(cascade = CascadeType.ALL)
	public Subject subject;

	@Constraints.Required
	@Constraints.MaxLength(50)
	@Column(unique=true)
	public String title;
	
	@Constraints.MaxLength(4096)
	@Column(columnDefinition="TEXT")
	public String body;
	public String coverImage;
	
	@URL
	public String resourceURL;
	
	public String youTubeVideoID;
	public Boolean share = true;
	
	@ManyToMany
	public List<Lesson> lessons = new ArrayList<Lesson>();
	
	public User author;
	
	public String getYouTubeVideoID()
	{
		String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
		Pattern compiledPattern = Pattern.compile(pattern);
		Matcher matcher = compiledPattern.matcher(this.resourceURL);
		if(matcher.find()){
	        return matcher.group();
	    }
		else
			return null;
	}
	
	public String getVimeoVideoID()
	{
		String result = this.resourceURL.replace("http://vimeo.com/", "");
		result = result.replace("https://vimeo.com/", "");				
		return result;
		
	}
	
	public boolean resourceIsYouTubeVideo()
	{
		return this.resourceURL.toLowerCase().contains("youtube.com");
	}
	
	public boolean resourceIsVimeoVideo()
	{
		return this.resourceURL.toLowerCase().contains("vimeo.com");
	}
	
	/**
     * Generic query helper for entity LessonElement with id String
     */
	public static final Finder<String, LessonElement> find = new Finder<String, LessonElement>(
			String.class, LessonElement.class);
	
	/**
     * Return a page of LessonElement
     *
     * @param page Page to display
     * @param pageSize Number of LessonElements per page
     * @param sortBy LessonElement property used for sorting
     * @param order Sort order (either or asc or desc)
     * @param filter Filter applied on the title column
     */
    public static Page<LessonElement> page(int page, int pageSize, String sortBy, String order, String filter) {
        return 
            find.where()
                .ilike("title", "%" + filter + "%")
                .orderBy(sortBy + " " + order)                
                .findPagingList(pageSize)
                .setFetchAhead(false)
                .getPage(page);
    }
 

    @Override
    public void update(Object o) {
        super.update(o);
    }
    
    public static Map<String, Boolean> makeLessonElementMap() {
	    Map<String, Boolean> lessonElementMap = new HashMap<String, Boolean>();
	    for (LessonElement element : LessonElement.find.findList()) {	    	
	    	lessonElementMap.put(element.title, false);
	    }
	    return lessonElementMap;
	  }
    
    public static Map<String, Boolean> makeLessonElementMap(LessonFormData lesson) {
	    Map<String, Boolean> lessonElementMap = new HashMap<String, Boolean>();
	    for (LessonElement element : LessonElement.find.findList()) {	    	
	    	lessonElementMap.put(element.title, (element != null && lesson.lessonElements.contains(element.title)));
	    }
	    return lessonElementMap;
	  }
}
