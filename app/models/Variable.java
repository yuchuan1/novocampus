package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
@Table(name = "Variables")
public class Variable extends Model{
	
  @Id
  private static final long serialVersionUID = 1L;

  @Required
	@Column(nullable = false, unique = true, length = 50)
	public String name;
	
	@Required
	public String value;

	public Variable(String name, String value) {
		this.name = name;
		this.value = value;
	}
	
	/**
   * Generic query helper for entity Variable with name String
   */
	public static final Finder<String, Variable> find = new Finder<String, Variable>(String.class, Variable.class);

	/**
   * Return value with name String
   */
	public static String getValue(String name){
		Variable variable = Variable.find.where().eq("name", name).findUnique();
  	String output = (variable==null) ? "" : variable.value;
		return output;
	}
}
