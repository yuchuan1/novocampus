package models;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import play.db.ebean.Model;

@Entity
@Table(name = "forums")
public class Forum extends Model{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public Forum() {
		super();
		this.id = UUID.randomUUID() + "";		
	}
	
	@Id
	public String id;
	public String name;
	
	@OneToOne(mappedBy = "forum")
	public Group group;
	
	@OneToMany(mappedBy = "forum")
	public List<Message> messages;

}
