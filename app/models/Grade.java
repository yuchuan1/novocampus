package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
@Table(name = "grades")
public class Grade extends BaseModel {

	private static final long serialVersionUID = 1L;

	@Id
	public String id;
	@Constraints.MaxLength(50)
	public String title;
	public Course course;
	public User student;
	public float score;
	@Constraints.MaxLength(50)
	public String instructorComment;
	
	
	/**
     * Generic query helper for entity Grade with id String
     */
	public static final Finder<String, Grade> find = new Finder<String, Grade>(
			String.class, Grade.class);
}
