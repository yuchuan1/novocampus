package models;

import java.util.Date;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import play.data.format.Formats.DateTime;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class Message extends Model{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public Message() {
		super();
		this.id = UUID.randomUUID() + "";
	}
	
	@Id
	public String id;
	
	@ManyToOne(cascade = CascadeType.ALL)
	public Forum forum;
	
	public String from_uid;
	
	public String to_uid;
	
	public String body;
	
	public Date sent;
	
	public static final Finder<String, Message> find = new Finder<String, Message>(
			String.class, Message.class);

}
