package models;

import org.apache.commons.lang3.RandomStringUtils;

import com.avaje.ebean.Page;
import com.google.common.base.Strings;

import play.data.validation.Constraints.*;

import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import models.enums.GradeLevel;
import models.enums.MembershipRole;
import models.User;
import views.formdata.GroupFormData;

@Entity
@Table(name = "groups")
public class Group extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	public String id;
	
	@Required
	public String name;

	@MaxLength(50)
	public String shortName;
	
	@Required
  @Column(nullable = false)
	public GradeLevel gradeLevel;
	
	// This means every group has exactly one schoolYear associated 
	@ManyToOne(cascade = CascadeType.ALL)
	public SchoolYear schoolYear;
	
	@ManyToOne(cascade = CascadeType.ALL)
	public User roomTeacher;

	public String groupCode;
	
  // This means one group can have many memberships associated
  @OneToMany(mappedBy = "group")
	public List<Membership> memberships;
	
	// This means one group can have many courses associated
  @OneToMany(mappedBy = "group")
  public List<Course> courses;
  
  @OneToOne
  @JoinColumn(name = "forum_id")
  public Forum forum;
	

	public Group() {
		super();
		this.id = UUID.randomUUID() + "";
	}
	
  public Group(String name, String shortName, GradeLevel gradeLevel, SchoolYear schoolYear, User roomTeacher) {
    super();
    this.id = UUID.randomUUID() + "";
    this.name = name;
    this.shortName = shortName;
    this.gradeLevel = gradeLevel;
    this.schoolYear = schoolYear;
    this.roomTeacher = roomTeacher;
  }
  
	/**
     * Generic query helper for entity Group with id String
     */
	public static final Finder<String, Group> find = new Finder<String, Group>(String.class, Group.class);
	
	/**
   * Return a page of SchoolYear
   *
   * @param page Page to display
   * @param pageSize Number of Group per page
   * @param sortBy Group property used for sorting
   * @param order Sort order (either or asc or desc)
   * @param filter Filter applied on the title column
   */
  public static Page<Group> page(int page, int pageSize, String sortBy, String order, String filter) {
    return find.where()
              .ilike("name", "%" + filter + "%")
              .orderBy(sortBy + " " + order)                
              .findPagingList(pageSize)
              .setFetchAhead(false)
              .getPage(page);
  }

  /**
   * Return an GroupFormData instance constructed from a group instance.
   * @param id The ID of a group instance.
   * @return The GroupFormData instance, or throws a RuntimeException. 
   */
  public static GroupFormData makeGroupFormData(String id) {
    try
    {     
      Group group = Group.find.byId(id);
      GroupFormData formData = new GroupFormData();
      formData.id = group.id;
      formData.name = group.name;
      formData.shortName = group.shortName;
      formData.gradeLevel = group.gradeLevel;
      formData.schoolYear = group.schoolYear.id;
      formData.roomTeacher = group.roomTeacher.id;
      return formData;
    }
    catch(Exception ex)
    {
      throw new RuntimeException("Couldn't find the group.");
    }
  }
  
  /**
   * Returns a group instance created from the form data.
   * Assumes that the formData has been validated.
   * The ID field is not assigned or managed in this application.
   * @param formData - The group form data.
   * @return A Group instance. 
   */
  public static Group makeInstance(GroupFormData formData) {
    Group group = Group.find.byId(formData.id);
    group.id = formData.id; 
    group.name = formData.name; 
    group.shortName = formData.shortName;
    group.gradeLevel = formData.gradeLevel;
    
    if(!Strings.isNullOrEmpty(formData.schoolYear))
      group.schoolYear = SchoolYear.find.byId(formData.schoolYear);

    if(!Strings.isNullOrEmpty(formData.roomTeacher))
      group.roomTeacher = User.find.byId(formData.roomTeacher);

    return group;
  }

  /**
   * Generate Group Code
   */
  public void generateGroupCode()
  {
    this.groupCode = RandomStringUtils.randomAlphanumeric(6);   
    while(Group.find.where().eq("groupCode", this.groupCode).findRowCount() > 0)
      this.groupCode = RandomStringUtils.randomAlphanumeric(6);
    this.save();      
  }

  /**
   * Remove Group Code
   */
  public void removeGroupCode()
  {
    this.groupCode = null;   
    this.save();      
  }

  /**
   * Add group member
   */
  public boolean addMember(User user) {
    return this.addMember(user, MembershipRole.Member);
  }
  
  public boolean addMember(User user, MembershipRole membershipRole) {
    Membership membership = new Membership(this, user, membershipRole);
    membership.save();
    return true;
  }

  /**
   * Delete group member
   */
  public boolean deleteMember(String uid) {
    String mid = Membership.find.where().eq("user_id", uid).eq("group_id", this.id).findUnique().id;
    if (Strings.isNullOrEmpty(mid)){
      return false;
    } else {
      Membership.find.ref(mid).delete();
      return true;
    }
  }
}
