package models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

import models.enums.Semester;
import models.Group;
import play.data.format.Formats;
import play.data.validation.Constraints.*;
import play.data.validation.ValidationError;
import play.i18n.Messages;

@Entity
@Table(
  name = "school_years",
  uniqueConstraints={@UniqueConstraint(columnNames={"academic_year", "semester"})}
)
public class SchoolYear extends BaseModel {

	private static final long serialVersionUID = 1L;
	
	@Id
	public String id;
	
	@Required
	@MaxLength(50)
  @Column(nullable = false, length = 50)
	public String academicYear;

	@Required
  @Column(nullable = false)
	public Semester semester;

	@Formats.DateTime(pattern = "yyyy-MM-dd")
	@Required
	public Date startDate;
	
	@Formats.DateTime(pattern = "yyyy-MM-dd")
	@Required
	public Date endDate;
	
	@MaxLength(50)
	public String description;

	// This means one schoolYear can have many groups associated
  @OneToMany(mappedBy = "schoolYear")
	public List<Group> groups;


	public SchoolYear() {
		super();
		this.id = UUID.randomUUID() + "";
	}
	
  public SchoolYear(String year, Semester semester, String startDate, String endDate, String description) throws ParseException {
    super();
    this.id = UUID.randomUUID() + "";
    this.academicYear = year;
    this.semester = semester;
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    this.startDate = formatter.parse(startDate);
    this.endDate = formatter.parse(endDate);
    this.description = description;
  }
  
	/**
   * Generic query helper for entity SchoolYear with id String
   */
	public static final Finder<String, SchoolYear> find = new Finder<String, SchoolYear>(String.class, SchoolYear.class);
	
	/**
   * Validate method is called after checking annotation-based constraints and only if they pass.
   * If validation passes you must return null.
   * Returning any not-null value (empty string or empty list) is treated as failed validation.
   * 
	 * @return errors or null
	 */
	public List<ValidationError> validate() {
    List<ValidationError> errors = new ArrayList<ValidationError>();
    if (startDate.after(endDate)){
      errors.add(new ValidationError("startDate", "The End date must be greater than the Start date."));
      errors.add(new ValidationError("endDate", "The End date must be greater than the Start date."));
    }
    return errors.isEmpty() ? null : errors;
	}
	
	public String title(){
	  return Messages.get("SchoolYear.short.title", this.academicYear, Messages.get("Semester." + this.semester));
	  //return this.description;
	}

  public String getFullTitle(){
    // files.summary=The disk {1} contains {0} file(s).
    // Messages.get("files.summary", d.files.length, d.name)
    // Messages.get(new Lang(Lang.forCode("tw")), "SchoolYear.full.title");
    // request().acceptLanguages()
    // System.out.println(request().acceptLanguages());
    return Messages.get("SchoolYear.full.title", this.academicYear, Messages.get("Semester." + this.semester));
    //return this.description;
  }

}
