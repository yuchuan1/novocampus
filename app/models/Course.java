package models;

import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.avaje.ebean.Page;

import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

import views.formdata.CourseFormData;


@Entity
@Table(name = "courses")
public class Course  extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	public String id;
	
	@Constraints.Required
	@Constraints.MaxLength(50)
	@Column(unique = true)
	public String title;
	
	@Constraints.MaxLength(4096)
	@Column(columnDefinition="TEXT")
	public String body;
	public Boolean enabled;
	public String coverImageUrl;
	
	// This means every course has exactly one group associated 
	@ManyToOne(cascade = CascadeType.ALL)
	public Group group;
	
	// This means every course has exactly one course module associated 
	@ManyToOne(cascade = CascadeType.ALL)
	public CourseModule courseModule;

	

	public Course() {
		super();
		this.id = UUID.randomUUID() + "";
	}
	
	/**
     * Generic query helper for entity Course with id String
     */
	public static final Finder<String, Course> find = new Finder<String, Course>(
			String.class, Course.class);
	
	/**
     * Return a page of courses
     *
     * @param page Page to display
     * @param pageSize Number of courses per page
     * @param sortBy Course property used for sorting
     * @param order Sort order (either or asc or desc)
     * @param filter Filter applied on the title column
     */
    public static Page<Course> page(int page, int pageSize, String sortBy, String order, String filter) {
        return 
            find.where()
                .ilike("title", "%" + filter + "%")
                .orderBy(sortBy + " " + order)                
                .findPagingList(pageSize)
                .setFetchAhead(false)
                .getPage(page);
    }
    
    /**
     * Return an CourseFormData instance constructed from a course instance.
     * @param id The ID of a course instance.
     * @return The CourseFormData instance, or throws a RuntimeException. 
     */
    public static CourseFormData makeCourseFormData(String id) {
    	try
    	{    
    		Course course = Course.find.byId(id);
    		CourseFormData formData = new CourseFormData();
    		
    		formData.body = course.body;
    		formData.courseModule = course.courseModule;
    		formData.coverImageUrl = course.coverImageUrl;
    		formData.enabled = course.enabled;
    		if(course.group != null)
    			formData.group =  course.group.name;
   			formData.id = course.id;
   			formData.title = course.title;
    	
	    	return formData;      
    	}
    	catch( Exception ex)
    	{
    		throw new RuntimeException("Couldn't find course");
    	}
    }
}
