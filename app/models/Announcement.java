package models;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.avaje.ebean.Page;

import play.data.validation.Constraints;
import play.db.ebean.Model;

@Entity
@Table(name = "announcements")
public class Announcement extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	public String id;
	
	@Constraints.Required
	@Constraints.MaxLength(50)
	public String title;
	
	@Constraints.MaxLength(4096)
	@Column(columnDefinition="TEXT")
	public String body;
	public String media;
	
	public Announcement() {
		super();
		this.id = UUID.randomUUID() + "";
	}
	
	/**
   * Generic query helper for entity Course with id String
   */
	public static final Finder<String, Announcement> find = new Finder<String, Announcement>(String.class, Announcement.class);
	
	/**
   * Return a page of Announcement
   *
   * @param page Page to display
   * @param pageSize Number of LessonElements per page
   * @param sortBy Announcement property used for sorting
   * @param order Sort order (either or asc or desc)
   * @param filter Filter applied on the title column
   */
  public static Page<Announcement> page(int page, int pageSize, String sortBy, String order, String filter) {
    return 
      find.where()
          .ilike("title", "%" + filter + "%")
          .orderBy(sortBy + " " + order)                
          .findPagingList(pageSize)
          .setFetchAhead(false)
          .getPage(page);
  }
}
