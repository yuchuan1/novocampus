package models;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
@Table(name = "assessment_items")
public class AssessmentItem  extends BaseModel{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	public String id;
	
	
	public AssessmentItem() {
		super();
		this.id = UUID.randomUUID() + "";
	}

	@Constraints.Required
	public String name;
	
	/**
     * Generic query helper for entity AssessmentItem with id String
     */
	public static final Finder<String, AssessmentItem> find = new Finder<String, AssessmentItem>(
			String.class, AssessmentItem.class);
}
