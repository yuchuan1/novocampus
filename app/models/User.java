package models;

import be.objectify.deadbolt.core.models.Permission;
import be.objectify.deadbolt.core.models.Role;
import be.objectify.deadbolt.core.models.Subject;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.ExpressionList;
import com.avaje.ebean.Page;
import com.feth.play.module.pa.providers.password.UsernamePasswordAuthUser;
import com.feth.play.module.pa.user.AuthUser;
import com.feth.play.module.pa.user.AuthUserIdentity;
import com.feth.play.module.pa.user.EmailIdentity;
import com.feth.play.module.pa.user.NameIdentity;
import com.feth.play.module.pa.user.FirstLastNameIdentity;
import com.google.common.base.Strings;

import models.TokenAction.Type;
import models.enums.Gender;
import models.enums.GradeLevel;
import models.enums.Relationship;
import models.enums.StudentStatus;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;
import play.data.validation.Constraints.*;
import play.data.format.Formats;
import views.formdata.UserFormData;

import javax.persistence.*;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.*;

/**
 * Initial version based on work by Steve Chaloner (steve@objectify.be) for
 * Deadbolt2
 */
@Entity
@Table(name = "users")
public class User extends BaseModel implements Subject {
  public User() {
    super();
    this.id = UUID.randomUUID() + "";
    this.roles = new ArrayList<>();
  }

  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;

  @Id
  @Required
  public String id;

  // if you make this unique, keep in mind that users *must* merge/link their
  // accounts then on signup with additional providers
  // @Column(unique = true)
  @Required
  @MaxLength(64)
  @Column(columnDefinition = "varchar(64)")
  @Email
  public String email;

  public boolean emailValidated;

  @Required
  @MaxLength(64)
  @Column(columnDefinition = "varchar(64)")
  public String loginID;

  @Required
  @MaxLength(64)
  @Column(columnDefinition = "varchar(64)")
  public String firstName;

  @Required
  @MaxLength(64)
  @Column(columnDefinition = "varchar(64)")
  public String lastName;

  @Required
  public Gender gender;

  @Required
  @Formats.DateTime(pattern = "yyyy-MM-dd")
  public Date birthDate;

  @Required
  public boolean active;

  @Formats.DateTime(pattern = "yyyy-MM-dd HH:mm:ss")
  public Date lastLogin;

  @MaxLength(2000)
  @Column(columnDefinition = "varchar(2000)")
  public String picture;

  @ManyToMany
  @JoinTable(name = "users_security_role")
  public List<SecurityRole> roles;
  
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
  public List<LinkedAccount> linkedAccounts;

  // teacher properties
  public String teacherID;

  // student properties
  @MaxLength(12)
  @MinLength(2)
  public String studentID;

  public GradeLevel gradeLevel;

  public StudentStatus studentStatus;

  @Column(columnDefinition = "char(6)")
  public String parentCode;

  // parent properties
  public Relationship relationship;

  // This means one group can have many memberships associated
  @OneToMany(mappedBy = "user")
  public List<Membership> memberships;

  public static final Finder<String, User> find = new Finder<String, User>(String.class, User.class);

  @Override
  public String getIdentifier() {
    // models.User.find.all()
    return this.id;
  }

  public String getFullName() {
    return this.lastName + this.firstName;
  }

  @Override
  public List<? extends Role> getRoles() {
    return roles;
  }

  @Override
  public List<? extends Permission> getPermissions() {
    List<Permission> permissions = new ArrayList<Permission>();

    for (SecurityRole r : roles) {
      for (Permission p : r.permissions) {
        if (!permissions.contains(p))
          permissions.add(p);
      }
    }

    return permissions;
  }

  public void GenerateParentCode() {
    this.parentCode = RandomStringUtils.randomAlphanumeric(6);
    while (User.find.where().eq("parentCode", this.parentCode).findRowCount() > 0)
      this.parentCode = RandomStringUtils.randomAlphanumeric(6);

    this.save();
  }

  public static boolean existsByAuthUserIdentity(final AuthUserIdentity identity) {
    final ExpressionList<User> exp;
    if (identity instanceof UsernamePasswordAuthUser) {
      exp = getUsernamePasswordAuthUserFind((UsernamePasswordAuthUser) identity);
    } else {
      exp = getAuthUserFind(identity);
    }
    return exp.findRowCount() > 0;
  }

  private static ExpressionList<User> getAuthUserFind(final AuthUserIdentity identity) {
    return find.where().eq("active", true).eq("linkedAccounts.providerUserId", identity.getId()).eq("linkedAccounts.providerKey", identity.getProvider());
  }

  public static User findByAuthUserIdentity(final AuthUserIdentity identity) {
    if (identity == null) {
      return null;
    }
    if (identity instanceof UsernamePasswordAuthUser) {
      return findByUsernamePasswordIdentity((UsernamePasswordAuthUser) identity);
    } else {
      return getAuthUserFind(identity).findUnique();
    }
  }

  public static User findByUsernamePasswordIdentity(final UsernamePasswordAuthUser identity) {
    return getUsernamePasswordAuthUserFind(identity).findUnique();
  }

  private static ExpressionList<User> getUsernamePasswordAuthUserFind(final UsernamePasswordAuthUser identity) {
    return getEmailUserFind(identity.getEmail()).eq("linkedAccounts.providerKey", identity.getProvider());
  }

  public void merge(final User otherUser) {
    for (final LinkedAccount acc : otherUser.linkedAccounts) {
      this.linkedAccounts.add(LinkedAccount.create(acc));
    }
    // do all other merging stuff here - like resources, etc.

    // deactivate the merged user that got added to this one
    otherUser.active = false;
    Ebean.save(Arrays.asList(new User[] { otherUser, this }));
  }

  public static User create(final AuthUser authUser) {
    final User user = new User();
    user.roles = Collections.singletonList(SecurityRole.findByRoleName(controllers.Application.USER_ROLE));
    // user.permissions = new ArrayList<UserPermission>();
    // user.permissions.add(UserPermission.findByValue("printers.edit"));
    user.active = true;
    user.lastLogin = new Date();
    user.linkedAccounts = Collections.singletonList(LinkedAccount.create(authUser));

    if (authUser instanceof EmailIdentity) {
      final EmailIdentity identity = (EmailIdentity) authUser;
      // Remember, even when getting them from FB & Co., emails should be
      // verified within the application as a security breach there might
      // break your security as well!
      user.email = identity.getEmail();
      user.emailValidated = false;
    }

    if (authUser instanceof NameIdentity) {
      final NameIdentity identity = (NameIdentity) authUser;
      final String name = identity.getName();
      if (name != null) {
        user.loginID = name;
      }
    }

    if (authUser instanceof FirstLastNameIdentity) {
      final FirstLastNameIdentity identity = (FirstLastNameIdentity) authUser;
      final String firstName = identity.getFirstName();
      final String lastName = identity.getLastName();
      if (firstName != null) {
        user.firstName = firstName;
      }
      if (lastName != null) {
        user.lastName = lastName;
      }
    }

    user.save();
    user.saveManyToManyAssociations("roles");
    // user.saveManyToManyAssociations("permissions");
    return user;
  }

  public static void merge(final AuthUser oldUser, final AuthUser newUser) {
    User.findByAuthUserIdentity(oldUser).merge(User.findByAuthUserIdentity(newUser));
  }

  public Set<String> getProviders() {
    final Set<String> providerKeys = new HashSet<String>(linkedAccounts.size());
    for (final LinkedAccount acc : linkedAccounts) {
      providerKeys.add(acc.providerKey);
    }
    return providerKeys;
  }

  public static void addLinkedAccount(final AuthUser oldUser, final AuthUser newUser) {
    final User u = User.findByAuthUserIdentity(oldUser);
    u.linkedAccounts.add(LinkedAccount.create(newUser));
    u.save();
  }

  public static void setLastLoginDate(final AuthUser knownUser) {
    final User u = User.findByAuthUserIdentity(knownUser);
    u.lastLogin = new Date();
    u.save();
  }

  public static User findByEmail(final String email) {
    return getEmailUserFind(email).findUnique();
  }

  private static ExpressionList<User> getEmailUserFind(final String email) {
    return find.where().eq("active", true).eq("email", email);
  }

  public LinkedAccount getAccountByProvider(final String providerKey) {
    return LinkedAccount.findByProviderKey(this, providerKey);
  }

  public static void verify(final User unverified) {
    // You might want to wrap this into a transaction
    unverified.emailValidated = true;
    unverified.save();
    TokenAction.deleteByUser(unverified, Type.EMAIL_VERIFICATION);
  }

  public void changePassword(final UsernamePasswordAuthUser authUser, final boolean create) {
    LinkedAccount a = this.getAccountByProvider(authUser.getProvider());
    if (a == null) {
      if (create) {
        a = LinkedAccount.create(authUser);
        a.user = this;
      } else {
        throw new RuntimeException("Account not enabled for password usage");
      }
    }
    a.providerUserId = authUser.getHashedPassword();
    a.save();
  }

  public void resetPassword(final UsernamePasswordAuthUser authUser, final boolean create) {
    // You might want to wrap this into a transaction
    this.changePassword(authUser, create);
    TokenAction.deleteByUser(this, Type.PASSWORD_RESET);
  }

  /**
   * Return a page of User
   * 
   * @param page
   *          Page to display
   * @param pageSize
   *          Number of Products per page
   * @param sortBy
   *          Product property used for sorting
   * @param order
   *          Sort order (either or asc or desc)
   * @param filter
   *          Filter applied on the name column
   */
  public static Page<User> page(int page, int pageSize, String sortBy, String order, String filter, String role, String gradeLevel) {
    Page<User> result = null;

    if (Strings.isNullOrEmpty(gradeLevel) && Strings.isNullOrEmpty(role)) {
    
      result = find.where().ilike("email", "%" + filter + "%").orderBy(sortBy + " " + order).findPagingList(pageSize).setFetchAhead(false).getPage(page);
    
    } else if (!Strings.isNullOrEmpty(gradeLevel)) {
      
      if (!Strings.isNullOrEmpty(role)) {
        result = find.where().ilike("email", "%" + filter + "%").eq("gradeLevel", GradeLevel.valueOf(gradeLevel)).eq("roles.id", role).orderBy(sortBy + " " + order).findPagingList(pageSize).setFetchAhead(false).getPage(page);
      } else {
        result = find.where().ilike("email", "%" + filter + "%").eq("gradeLevel", GradeLevel.valueOf(gradeLevel)).orderBy(sortBy + " " + order).findPagingList(pageSize).setFetchAhead(false).getPage(page);
      }
    
    } else if (!Strings.isNullOrEmpty(role)) {

      if (!Strings.isNullOrEmpty(gradeLevel)) {
        result = find.where().ilike("email", "%" + filter + "%").eq("gradeLevel", GradeLevel.valueOf(gradeLevel)).eq("roles.id", role).orderBy(sortBy + " " + order).findPagingList(pageSize).setFetchAhead(false).getPage(page);
      } else {
        result = find.where().ilike("email", "%" + filter + "%").eq("roles.id", role).orderBy(sortBy + " " + order).findPagingList(pageSize).setFetchAhead(false).getPage(page);
      }
    
    }

    return result;

  }

  /**
   * Returns a User instance created from the form data. Assumes that the
   * formData has been validated. The ID field is not assigned or managed in
   * this application.
   * 
   * @param formData
   *          The user form data.
   * @return An user instance.
   */
  public static User makeInstance(UserFormData formData) {

    User user = User.find.byId(formData.id);

    user.active = formData.active;
    user.email = formData.email;
    user.firstName = formData.firstName;
    user.gender = formData.gender;
    user.lastName = formData.lastName;
    user.loginID = formData.loginID;
    if (!Strings.isNullOrEmpty(formData.picture))
      user.picture = formData.picture;
    user.studentID = formData.studentId;
    user.gradeLevel = formData.gradeLevel;
    user.studentStatus = formData.studentStatus;
    user.teacherID = formData.teacherID;
    user.relationship = formData.relationship;
    user.birthDate = formData.birthDate;

    Ebean.deleteManyToManyAssociations(user, "roles");

    for (String role : formData.roles) {
      user.roles.add(SecurityRole.findByRoleName(role));
    }

    return user;
  }

  /**
   * Return an UserFormData instance constructed from an user instance.
   * 
   * @param id
   *          The ID of a user instance.
   * @return The UserFormData instance, or throws a RuntimeException.
   */
  public static UserFormData makeUserFormData(String id) {
    try {
      User user = User.find.byId(id);
      UserFormData formData = new UserFormData();
      formData.id = user.id;
      formData.loginID = user.loginID;
      formData.active = user.active;
      formData.email = user.email;
      formData.firstName = user.firstName;
      formData.gender = user.gender;
      formData.lastName = user.lastName;
      if (!Strings.isNullOrEmpty(user.picture))
        formData.picture = user.picture;
      else
        formData.picture = "public/img/defaultUser.png";
      formData.studentId = user.studentID;
      formData.studentStatus = user.studentStatus;
      formData.gradeLevel = user.gradeLevel;
      formData.teacherID = user.teacherID;
      formData.relationship = user.relationship;
      formData.birthDate = user.birthDate;

      // formData.roles

      for (SecurityRole role : user.roles) {
        formData.roles.add(role.roleName);
      }

      return formData;
    } catch (Exception ex) {
      throw new RuntimeException("Couldn't find user");
    }
  }

  public static Map<String, String> orderBy() {
    LinkedHashMap<String, String> options = new LinkedHashMap<String, String>();

    options.put("email", "Email");
    options.put("created_at", "Created Date");
    options.put("loginID", "Login ID");

    return options;
  }

  public static Map<String, Boolean> makeUserMap() {
    Map<String, Boolean> userMap = new HashMap<String, Boolean>();
    for (User user : User.find.findList()) {
      userMap.put(user.id, false);
    }
    return userMap;
  }

}
