package models;

import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import play.db.ebean.Model;
import play.db.ebean.Model.Finder;
import play.data.validation.Constraints.*;
import models.Group;
import models.enums.MembershipRole;

@Entity
@Table(name = "memberships")
public class Membership extends BaseModel{
	
	private static final long serialVersionUID = 1L;	
	@Id
	public String id;

  // This means every membership has exactly one group associated 
  @ManyToOne(cascade = CascadeType.ALL)
  @Required
  public Group group;
	
  // This means every membership has exactly one user associated 
  @ManyToOne(cascade = CascadeType.ALL)
  @Required
  public User user;
	
  @Required
  public MembershipRole role;   // Admin or Member
	
  
  /**
   * Initialize this object
   */
  public Membership(Group group, User user) {
    this(group, user, MembershipRole.Member);
  }

  // public Membership(String gid, String uid) {
  // Group group = Group.find.byId(gid);
  // User user = User.find.byId(uid);
  // this(group, user, MembershipRole.Member);
  // }

  public Membership(Group group, User user, MembershipRole membershipRole) {
    super();
    this.id = UUID.randomUUID() + "";
    this.group = group;
    this.user = user;
    this.role = membershipRole;
  }
  
  
  /**
   * Generic query helper for entity Group with id String
   */
  public static final Finder<String, Membership> find = new Finder<String, Membership>(String.class, Membership.class);

}
