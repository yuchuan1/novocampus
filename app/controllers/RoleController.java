package controllers;

import models.SecurityRole;
import models.UserPermission;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import utils.BreadCrumb;
import views.formdata.RoleFormData;
import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;

public class RoleController extends Controller {
	
	/**
	 * This result directly redirect to application home.
	 */
	public static Result GO_HOME = redirect(routes.RoleController.list(0, "roleName",
			"asc", ""));
	
	/**
     * Handle default path requests, redirect to lesson elements list
     */
    public static Result index() {
        return GO_HOME;
    }
	
	/**
	 * Display the paginated list of role.
	 * 
	 * @param page
	 *            Current page number (starts from 0)
	 * @param sortBy
	 *            Column to be sorted
	 * @param order
	 *            Sort order (either asc or desc)
	 * @param filter
	 *            Filter applied on email
	 */
	public static Result list(int page, String sortBy, String order, String filter) {
		BreadCrumb breadCrumb = new BreadCrumb("Roles");
		breadCrumb.Add("Home", "/");
		
		return ok(
				views.html.role.list.render(SecurityRole.page(page, 10, sortBy, order, filter), sortBy, order,filter, breadCrumb)
		);
	}

	/**
     * Display the 'edit form' of an existing role.
     *
     * @param id Id of the role to edit
     */
	@Restrict(@Group(Application.USER_ROLE))
    public static Result edit(String id) {
		BreadCrumb breadCrumb = new BreadCrumb("Edit");
		breadCrumb.Add("Home", "/");
		breadCrumb.Add("Roles", "/roles");
		
		RoleFormData roleData = SecurityRole.makeRoleFormData(id);
		Form<RoleFormData> formData = Form.form(RoleFormData.class).fill(roleData);

        return ok(
            views.html.role.editForm.render(id, formData, UserPermission.makePermissionMap(roleData), breadCrumb)
        );
    }
	
	/**
     * Handle the 'edit form' submission 
     *
     * @param id Id of the role to edit
     */
	@Restrict(@Group(Application.USER_ROLE))
    public static Result update(String id) {
		BreadCrumb breadCrumb = new BreadCrumb("Edit");
		breadCrumb.Add("Home", "/");
		breadCrumb.Add("Roles", "/roles");
		
		// Get the submitted form data from the request object, and run validation.
	    Form<RoleFormData> roleData = Form.form(RoleFormData.class).bindFromRequest();
	    
	    if(roleData.hasErrors()) {
            return badRequest(
            	views.html.role.editForm.render(id, roleData, UserPermission.makePermissionMap(), breadCrumb)
            		);
        }
        else
        {
        	// Convert the formData into a role model instance.        	
            SecurityRole role = SecurityRole.makeInstance(roleData.get());
            role.saveManyToManyAssociations("permissions");
            role.update();
            
            flash("success", "Role " + role.roleName + " has been updated");       
           
        }
	    
	    return GO_HOME;
	}
	
	
}
