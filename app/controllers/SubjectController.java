package controllers;

import static play.data.Form.form;
import models.Subject;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import utils.BreadCrumb;

public class SubjectController extends Controller{
	/**
	 * This result directly redirect to application home.
	 */
	public static Result GO_HOME = redirect(routes.AnnouncementController.list(0, "title", "asc", ""));
	
	/**
     * Handle default path requests, redirect to Subject list
     */
  public static Result index() {
  	return GO_HOME;
  }

	/**
	 * Display the paginated list of Subjects.
	 * 
	 * @param page
	 *            Current page number (starts from 0)
	 * @param sortBy
	 *            Column to be sorted
	 * @param order
	 *            Sort order (either asc or desc)
	 * @param filter
	 *            Filter applied on Subject names
	 */
	public static Result list(int page, String sortBy, String order, String filter) {
		BreadCrumb breadCrumb = new BreadCrumb("Subjects");
		breadCrumb.Add("Home", "/");
		
		return ok(
				views.html.subject.list.render(Subject.page(page, 10, sortBy, order, filter), sortBy, order, filter, breadCrumb)
				);
	}

	/**
	 * Display the 'new Subject form'.
	 */
	public static Result create() {
		BreadCrumb breadCrumb = new BreadCrumb("New");
		breadCrumb.Add("Home", "/");
		breadCrumb.Add("Subjects", "/subjects");
		Form<Subject> subjectForm = form(Subject.class);
		return ok(
				views.html.subject.createForm.render(subjectForm, breadCrumb)
				);
	}

	/**
	 * Handle the 'new Subject form' submission
	 */

	public static Result save() {
		BreadCrumb breadCrumb = new BreadCrumb("New");
		breadCrumb.Add("Home", "/");
		breadCrumb.Add("Subjects", "/subjects");
		
		Form<Subject> SubjectForm = form(Subject.class).bindFromRequest();
		if (SubjectForm.hasErrors()) {
			return badRequest(
					views.html.subject.createForm.render(SubjectForm, breadCrumb)
					);
		}
		SubjectForm.get().save();
		flash("success", "Subject " + SubjectForm.get().name+ " has been created");
		return GO_HOME;
	}
	
	 /**
     * Display the 'edit form' of a existing Subject.
     *
     * @param id Id of the Subject to edit
     */
    public static Result edit(String id) {
    	BreadCrumb breadCrumb = new BreadCrumb("Edit");
		breadCrumb.Add("Home", "/");
		breadCrumb.Add("Subjects", "/subjects");
		
        Form<Subject> editForm = form(Subject.class).fill(
        		Subject.find.byId(id)
        );
        return ok(
            views.html.subject.editForm.render(id, editForm, breadCrumb)
        );
    }
    
  /**
   * Handle the 'edit form' submission 
   *
   * @param id Id of the Subject to edit
   */
  public static Result update(String id) {
	  BreadCrumb breadCrumb = new BreadCrumb("Edit");
		breadCrumb.Add("Home", "/");
		breadCrumb.Add("Subjects", "/subjects");
		
      Form<Subject> SubjectForm = form(Subject.class).bindFromRequest();
      if(SubjectForm.hasErrors()) {
          return badRequest(
          		views.html.subject.editForm.render(id, SubjectForm, breadCrumb)
          );
      }
      SubjectForm.get().update((Object)id);
      flash("success", "Subject " + SubjectForm.get().name + " has been updated");
      return GO_HOME;
  }
	
	/**
     * Handle Subject deletion
     */
    public static Result delete(String id) {
    	Subject.find.ref(id).delete();
        flash("success", "Subject has been deleted");
        return GO_HOME;
    }
}
