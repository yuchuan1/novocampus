package controllers;

import java.text.SimpleDateFormat;
import java.util.Date;

import play.Play;
import play.Routes;
import play.data.Form;
import play.mvc.*;
import play.mvc.Http.Session;
import providers.MyUsernamePasswordAuthProvider;
import providers.MyUsernamePasswordAuthProvider.MyLogin;
import providers.MyUsernamePasswordAuthProvider.MySignup;
import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;

import com.feth.play.module.pa.PlayAuthenticate;
import com.feth.play.module.pa.providers.password.UsernamePasswordAuthProvider;
import com.feth.play.module.pa.user.AuthUser;

import models.User;
import views.html.*;

public class Application extends Controller {

  public static final String FLASH_MESSAGE_KEY = "message";
  public static final String FLASH_SUCCESS_KEY = "success";
  public static final String FLASH_ERROR_KEY = "error";
  public static final String USER_ROLE = "User";
  public static final String ADMIN_ROLE = "Admin";
  public static final String TEACHER_ROLE = "Teacher";
  public static final String STUDENT_ROLE = "Student";
  public static final String PARENT_ROLE = "Parent";

  public static Result index() {
    final User localUser = getLocalUser(session());
    if (localUser==null)
      return login();
    else
      return ok(index.render());
  }

  public static User getLocalUser(final Session session) {
    final AuthUser currentAuthUser = PlayAuthenticate.getUser(session);
    final User localUser = User.findByAuthUserIdentity(currentAuthUser);
    return localUser;
  }

  public static User getLocalUser() {
    User localUser;
    try {
      localUser = User.findByAuthUserIdentity(PlayAuthenticate.getUser(session()));
      return localUser;
    } catch (RuntimeException ex) {
      if (ex.getMessage().equals("There is no HTTP Context available from here.")) {
        localUser = User.findByEmail("admin@novocampus.com");
        return localUser;
      }

      return null;
    }

  }

  @Restrict(@Group(Application.USER_ROLE))
  public static Result restricted() {
    final User localUser = getLocalUser(session());
    return ok(restricted.render(localUser));
  }

  @Restrict(@Group(Application.USER_ROLE))
  public static Result profile() {
    final User localUser = getLocalUser(session());
    return redirect(routes.UserController.view(localUser.id));
    //return ok(profile.render(localUser));
  }

  public static Result login() {
    return ok(login.render(MyUsernamePasswordAuthProvider.LOGIN_FORM));
  }

  public static Result doLogin() {
    com.feth.play.module.pa.controllers.Authenticate.noCache(response());
    final Form<MyLogin> filledForm = MyUsernamePasswordAuthProvider.LOGIN_FORM.bindFromRequest();
    if (filledForm.hasErrors()) {
      // User did not fill everything properly
      return badRequest(login.render(filledForm));
    } else {
      // Everything was filled
      return UsernamePasswordAuthProvider.handleLogin(ctx());
    }
  }

  public static Result signup() {
    return ok(signup.render(MyUsernamePasswordAuthProvider.SIGNUP_FORM));
  }

  public static Result jsRoutes() {
    return ok(Routes.javascriptRouter("jsRoutes", controllers.routes.javascript.Signup.forgotPassword())).as("text/javascript");
  }

  public static Result doSignup() {
    com.feth.play.module.pa.controllers.Authenticate.noCache(response());
    final Form<MySignup> filledForm = MyUsernamePasswordAuthProvider.SIGNUP_FORM.bindFromRequest();
    if (filledForm.hasErrors()) {
      // User did not fill everything properly
      return badRequest(signup.render(filledForm));
    } else {
      // Everything was filled
      // do something with your part of the form before handling the user
      // signup

      return UsernamePasswordAuthProvider.handleSignup(ctx());
      // return MyUsernamePasswordAuthProvider.handleSignup(ctx());
    }
  }

  public static String formatTimestamp(final long t) {
    return new SimpleDateFormat("yyyy-dd-MM HH:mm:ss").format(new Date(t));
  }
  
  public static Result chatRoom() {
	  
    final User localUser = Application.getLocalUser(session());
    return ok(views.html.chatRoom.index.render(localUser));
    //return ok(views.html.user.view.render(localUser, viewUser, breadCrumb));
    //return ok("Testing XMPP chat function");
	}

}