package controllers;

import models.LessonElement;
import play.mvc.*;
import play.data.*;
import play.mvc.Result;
import utils.BreadCrumb;
import models.*;
import models.enums.GradeLevel;

import static play.data.Form.*;

public class LessonElementController extends Controller {

	/**
	 * This result directly redirect to application home.
	 */
	public static Result GO_HOME = redirect(routes.LessonElementController.list(0, "title",
			"asc", ""));
	
	/**
     * Handle default path requests, redirect to lesson elements list
     */
    public static Result index() {
        return GO_HOME;
    }

	/**
	 * Display the paginated list of lesson elements.
	 * 
	 * @param page
	 *            Current page number (starts from 0)
	 * @param sortBy
	 *            Column to be sorted
	 * @param order
	 *            Sort order (either asc or desc)
	 * @param filter
	 *            Filter applied on lesson element titles
	 */
	public static Result list(int page, String sortBy, String order,
			String filter) {
		BreadCrumb breadCrumb = new BreadCrumb("Lesson Elements");
		breadCrumb.Add("Home", "/");
		return ok(
				views.html.lessonElement.list.render(LessonElement.page(page, 10, sortBy, order, filter), sortBy, order, filter, breadCrumb)

		);
	}
	
	 /**
     * Display the 'edit form' of a existing lesson element.
     *
     * @param id Id of the lesson element to edit
     */
    public static Result edit(String id) {
    	BreadCrumb breadCrumb = new BreadCrumb("Edit");
		breadCrumb.Add("Home", "/");
		breadCrumb.Add("Lesson Elements", "/lessonElements");
		
    	LessonElement element = LessonElement.find.byId(id);
        Form<LessonElement> editForm = form(LessonElement.class).fill(element);
        return ok(
           views.html.lessonElement.editForm.render(id, editForm, element, breadCrumb)
        );
    }
    
    /**
     * Handle the 'edit form' submission 
     *
     * @param id Id of the lesson element to edit
     */
    public static Result update(String id) {
    	LessonElement element = LessonElement.find.byId(id);
        Form<LessonElement> lessonElementForm = form(LessonElement.class).bindFromRequest();
        
        element.author = Application.getLocalUser(session());
		element.body = lessonElementForm.data().get("body");
		element.gradeLevel = GradeLevel.valueOf(lessonElementForm.data().get("gradeLevel"));
		element.resourceURL = lessonElementForm.data().get("resourceURL");
		element.share = Boolean.valueOf(lessonElementForm.data().get("share"));
		element.subject = Subject.find.where().eq("id", lessonElementForm.data().get("subject.id")).findUnique();
		element.title = lessonElementForm.data().get("title");
		element.update();
        /*
        if(lessonElementForm.hasErrors()) {
            return badRequest(
            		views.html.lessonElement.editForm.render(id, lessonElementForm, element)
            		);
        }
        */
       // lessonElementForm.get().update((Object)id);
        flash("success", "Lesson Element " + lessonElementForm.get().title + " has been updated");
        return GO_HOME;
    }

	/**
	 * Display the 'new lesson element form'.
	 */
	public static Result create() {
		Form<LessonElement> lessonElementForm = form(LessonElement.class);
		
		BreadCrumb breadCrumb = new BreadCrumb("Add");
		breadCrumb.Add("Home", "/");
		breadCrumb.Add("Lesson Elements", "/lessonElements");
	
		return ok(
				views.html.lessonElement.createForm.render(lessonElementForm, breadCrumb)
				);
	}

	/**
	 * Handle the 'new lesson element form' submission
	 */

	public static Result save() {
		Form<LessonElement> lessonElementForm = form(LessonElement.class)
				.bindFromRequest();
		
		LessonElement element = new LessonElement();
		element.author = Application.getLocalUser(session());
		element.body = lessonElementForm.data().get("body");
		element.gradeLevel = GradeLevel.valueOf(lessonElementForm.data().get("gradeLevel"));
		element.resourceURL = lessonElementForm.data().get("resourceURL");
		element.share = Boolean.valueOf(lessonElementForm.data().get("share"));
		element.subject = Subject.find.where().eq("id", lessonElementForm.data().get("subject.id")).findUnique();
		element.title = lessonElementForm.data().get("title");
		
		/*
		if (lessonElementForm.hasErrors()) {
			return badRequest(views.html.lessonElement.createForm
					.render(lessonElementForm));
		}
		*/
		
		element.save();
		flash("success", "Lesson Element " + lessonElementForm.get().title
				+ " has been created");
		return GO_HOME;
	}
	
	/**
     * Handle lesson element deletion
     */
    public static Result delete(String id) {
        LessonElement.find.ref(id).delete();
        flash("success", "Lesson Element has been deleted");
        return GO_HOME;
    }

}
