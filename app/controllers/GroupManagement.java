/**
 * for school admin role 
 */
package controllers;

import java.util.List;

import models.Group;
import play.mvc.Controller;
import play.mvc.Result;
import utils.BreadCrumb;

/**
 * @author MOLLY.H
 *
 */
public class GroupManagement extends Controller {
 
  /**
   * This result directly redirect to application home.
   */
  public static Result GO_HOME = redirect(routes.GroupManagement.list());
  public static String nav = "/groupManagement";

  /**
   * Display the paginated list of groups.
   * 
   * @param page
   *          Current page number (starts from 0)
   * @param sortBy
   *          Column to be sorted
   * @param order
   *          Sort order (either asc or desc)
   * @param filter
   *          Filter applied on group titles
   */
  public static Result list() {
    String currentSchoolYearID = SchoolYearController.getCurrentSchoolYearID();
    List<Group> groups = (currentSchoolYearID=="") ? Group.find.all() : Group.find.where().eq("school_year_id", currentSchoolYearID).findList();
    BreadCrumb breadCrumb = new BreadCrumb("groupManagement.list.title", nav);
    breadCrumb.nav = nav;
    return ok(views.html.groupManagement.list.render(groups, breadCrumb));
  }
}
