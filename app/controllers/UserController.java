package controllers;

import static play.data.Form.form;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import com.csvreader.CsvReader;
import com.google.common.base.Strings;

import models.SecurityRole;
import models.User;
import models.enums.Gender;
import models.enums.GradeLevel;
import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import play.Play;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.Result;
import providers.MyUsernamePasswordAuthUser;
import providers.MyUsernamePasswordAuthProvider.MySignup;
import service.MyUserServicePlugin;
import utils.BreadCrumb;
import views.formdata.UserFormData;
import views.formdata.UserSearchFormData;


public class UserController extends Controller {

  /**
   * This result directly redirect to application home.
   */
  public static Result GO_HOME = redirect(routes.UserController.list(0, "loginID", "asc", ""));
  public static String nav = "/users";

  /**
   * Handle default path requests, redirect to lesson elements list
   */
  public static Result index() {
    return GO_HOME;
  }

  static String playPath = Play.application().path().toString();

  /**
   * Display the paginated list of users.
   * 
   * @param page
   *          Current page number (starts from 0)
   * @param sortBy
   *          Column to be sorted
   * @param order
   *          Sort order (either asc or desc)
   * @param filter
   *          Filter applied on email
   */
  //@Restrict(@Group(Application.ADMIN_ROLE))
  public static Result list(int page, String sortBy, String order, String filter) {

    String role = "";
    String gradeLevel = "";

    BreadCrumb breadCrumb = new BreadCrumb("user.list.title", nav);

    Form<UserSearchFormData> searchFormData = form(UserSearchFormData.class).bindFromRequest();
    if (!Strings.isNullOrEmpty(searchFormData.get().orderBy))
      sortBy = searchFormData.get().orderBy;

    if (!Strings.isNullOrEmpty(searchFormData.get().role))
      // role = SecurityRole.find.byId(searchFormData.get().role).roleName;
      role = searchFormData.get().role;

    if (!Strings.isNullOrEmpty(searchFormData.get().gradeLevel))
      gradeLevel = searchFormData.get().gradeLevel;

    // Page<User> currentPage = User.page(page, 10, sortBy, order, filter, role, gradeLevel);
    // PageHelper pageHelper = new PageHelper("User", sortBy, order, filter, currentPage.getPageIndex(), currentPage.getTotalPageCount(), currentPage.getTotalRowCount(), currentPage.hasPrev(), currentPage.hasNext());
    // return ok(views.html.user.list.render(currentPage, pageHelper, breadCrumb, searchFormData));
    return ok(views.html.user.list.render(User.page(page, 10, sortBy, order, filter, role, gradeLevel), sortBy, order, filter, breadCrumb, searchFormData));
  }

  /**
   * Display the 'new user form'.
   */
  @Restrict(@Group(Application.ADMIN_ROLE))
  public static Result create(String part) {
    BreadCrumb breadCrumb = new BreadCrumb("Add a " + part, routes.UserController.create(part).url());
    breadCrumb.Add("user.list.title", nav);

    UserFormData userData = new UserFormData();
    userData.active = true;
    userData.gender = Gender.Male;
    userData.roles = new ArrayList<String>();
    userData.roles.add(part);
    
    Form<UserFormData> userFormData = form(UserFormData.class).fill(userData);
    return ok(views.html.user.createForm.render(part, userFormData, SecurityRole.makeRoleMap(userData), breadCrumb));
    // return ok(views.html.user.createForm.render(part, userFormData, SecurityRole.makeRoleMap(), breadCrumb));
  }

  /**
   * Handle the 'new user form' submission
   */
  @Restrict(@Group(Application.ADMIN_ROLE))
  public static Result save(String part) {
    BreadCrumb breadCrumb = new BreadCrumb("Add");
    breadCrumb.Add("user.list.title", nav);

    Form<UserFormData> userFormData = form(UserFormData.class).bindFromRequest();

    //UserFormData userData = new UserFormData();
    if (userFormData.hasErrors()) {
      return badRequest(views.html.user.createForm.render(part, userFormData, SecurityRole.makeRoleMap(), breadCrumb));
    }

    User user = new User();
    user.active = true;
    user.email = userFormData.get().email;
    user.emailValidated = false;
    user.firstName = userFormData.get().firstName;
    user.gender = userFormData.get().gender;
    user.lastName = userFormData.get().lastName;
    user.loginID = userFormData.get().loginID;
    if (!Strings.isNullOrEmpty(userFormData.get().picture))
      user.picture = userFormData.get().picture;
    else
      user.picture = "files/defaultUser.png";

    user.birthDate = userFormData.get().birthDate;
    user.roles = new ArrayList<>();
    user.roles.clear();
    user.roles.add(SecurityRole.find.where().eq("roleName", Application.USER_ROLE).findUnique());

    for (String role : userFormData.get().roles) {
      user.roles.add(SecurityRole.findByRoleName(role));
    }

    user.studentID = userFormData.get().studentId;
    user.studentStatus = userFormData.get().studentStatus;
    user.gradeLevel = userFormData.get().gradeLevel;

    user.teacherID = userFormData.get().teacherID;
    user.relationship = userFormData.get().relationship;

    user.save();

    flash(Application.FLASH_SUCCESS_KEY, "User " + userFormData.get().firstName + userFormData.get().lastName + " has been created");

    MultipartFormData body = request().body().asMultipartFormData();
    FilePart picture = body.getFile("picture");

    if (picture != null && ( picture.getContentType().equals("image/png") || picture.getContentType().equals("image/jpg") || picture.getContentType().equals("image/jpeg") || picture.getContentType().equals("image/gif") )) {
      String fileName = picture.getFilename();

      File file = picture.getFile();
      try {
        user = User.find.where().eq("id", userFormData.get().id).findUnique();

        String ext = "." + FilenameUtils.getExtension(fileName);
        File f = new File("files/user/images", user.id + ext);

        if (f.exists())
          f.delete();

        FileUtils.moveFile(file, new File("files/user/images/", user.id + ext));

        user.picture = "files/user/images/" + user.id + ext;
        user.update();

      } catch (Exception e) {
        e.printStackTrace();
      }

      return GO_HOME;
    } else {
      return badRequest(views.html.user.createForm.render(part, userFormData, SecurityRole.makeRoleMap(), breadCrumb));
    }

  }

  /**
   * Display the picture of an existing user.
   * 
   * @param id
   *          Id of the user
   */
  public static Result picture(String id) {
    User user = User.find.where().eq("id", id).findUnique();
    File f;
    if (!Strings.isNullOrEmpty(user.picture)) {
      f = new File(user.picture);
    } else
      f = new File("files/defaultUser.png");

    return ok(f);
  }

  /**
   * Display all information of an existing user.
   * 
   * @param id
   *          Id of the user
   */
  public static Result view(String id) {
    final User localUser = Application.getLocalUser(session());
    User viewUser = User.find.where().eq("id", id).findUnique();

    BreadCrumb breadCrumb = new BreadCrumb(viewUser.getFullName(), routes.UserController.view(viewUser.id).url());
    breadCrumb.nav = nav;
    breadCrumb.destination = nav;
    breadCrumb.Add("user.list.title", nav);

    return ok(views.html.user.view.render(localUser, viewUser, breadCrumb));
  }

  /**
   * Display the 'edit form' of an existing user.
   * 
   * @param id
   *          Id of the user to edit
   */
  @Restrict(@Group(Application.USER_ROLE))
  public static Result edit(String id) {
    UserFormData userData = User.makeUserFormData(id);
    Form<UserFormData> formData = Form.form(UserFormData.class).fill(userData);

    BreadCrumb breadCrumb = new BreadCrumb("Edit");
    breadCrumb.nav = nav;
    breadCrumb.destination = routes.UserController.edit(id).url();
    breadCrumb.Add("user.list.title", nav);
    breadCrumb.Add(userData.lastName+userData.firstName, routes.UserController.view(id).url());
    
    return ok(views.html.user.editForm.render(id, formData, SecurityRole.makeRoleMap(userData), breadCrumb));
  }

  /**
   * Handle the 'edit form' submission
   * 
   * @param id
   *          Id of the user to edit
   */
  @Restrict(@Group(Application.USER_ROLE))
  public static Result update(String id) {
    BreadCrumb breadCrumb = new BreadCrumb("Update");
    breadCrumb.Add("user.list.title", nav);
    // Get the submitted form data from the request object, and run validation.
    Form<UserFormData> userData = Form.form(UserFormData.class).bindFromRequest();

    if (userData.hasErrors()) {
      return badRequest(views.html.user.editForm.render(id, userData, SecurityRole.makeRoleMap(), breadCrumb));
    } else {
      /*
       * Convert the formData into an user model instance. User user =
       * User.find.byId(id); user.active = userData.get().active; user.birthDate
       * = userData.get().birthDate; user.email = userData.get().email;
       * user.firstName = userData.get().firstName; user.gender =
       * userData.get().gender; user.gradeLevel = userData.get().gradeLevel;
       * user.lastName = userData.get().lastName; user.loginID =
       * userData.get().loginID; user.picture = userData.get().picture;
       * user.relationship = userData.get().relationship; user.studentID =
       * userData.get().studentId; user.studentStatus =
       * userData.get().studentStatus; user.teacherID =
       * userData.get().teacherID; user.saveManyToManyAssociations("roles");
       */
      User user = User.makeInstance(userData.get());
      // user.saveManyToManyAssociations("roles");
      // user.refresh();
      user.update();
      flash(Application.FLASH_SUCCESS_KEY, "User " + user.getFullName() + " has been updated");

      MultipartFormData body = request().body().asMultipartFormData();
      FilePart picture = body.getFile("picture");

      if (picture != null && (picture.getContentType().equals("image/png") || picture.getContentType().equals("image/jpg") || picture.getContentType().equals("image/jpeg") || picture.getContentType().equals("image/gif"))) {
        String fileName = picture.getFilename();
        File file = picture.getFile();
        try {
          user = User.find.where().eq("id", id).findUnique();

          String ext = "." + FilenameUtils.getExtension(fileName);
          File f = new File("files/user/images/", user.id + ext);

          if (f.exists())
            f.delete();

          FileUtils.moveFile(file, new File("files/user/images/", user.id + ext));

          user.picture = "files/user/images/" + user.id + ext;
          user.update();

        } catch (Exception e) {
          e.printStackTrace();
        }
      }

      return GO_HOME;
    }

  }

  /**
   * Handle group remove form
   */
  @Restrict(@Group(Application.ADMIN_ROLE))
  public static Result remove(String id) {
    User user = User.find.ref(id);
    String destination = request().getQueryString("destination").toString();
    
    BreadCrumb breadCrumb = new BreadCrumb("Delete");
    breadCrumb.nav = nav;
    breadCrumb.destination = destination;
    breadCrumb.Add("user.list.title", nav);
    breadCrumb.Add(user.getFullName(), routes.UserController.view(id).url());
    
    return ok(views.html.tags.removeForm.render(user.getFullName(), routes.UserController.delete(id).url(), breadCrumb));
  }


  /**
   * Handle user deletion
   */
  @Restrict(@Group(Application.ADMIN_ROLE))
  public static Result delete(String id) {
    User user = User.find.ref(id);

    user.deleteManyToManyAssociations("roles");
    user.delete();

    flash(Application.FLASH_SUCCESS_KEY, "User has been deleted");
    return GO_HOME;

  }

  // import user from csv file
  @Restrict(@Group(Application.ADMIN_ROLE))
  public static Result importUser() {
    MultipartFormData body = request().body().asMultipartFormData();
    FilePart csvfile = body.getFile("csvimport");
    String fileName = csvfile.getFilename();
    String contentType = csvfile.getContentType();
    File file = csvfile.getFile();
    String csv = "";

    try {
      csv = FileUtils.readFileToString(file, "UTF-8");
    } catch (IOException e1) {
      e1.printStackTrace();
    }

    /*
     * String csv =
     * "StudentID, UserName, Email, FirstName, LastName, Gender, GradeLevel, Picture, Role, Status\n"
     * +
     * "1234567, username, user@novocampus.com, Walter, White, MALE, Twelve, https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcRIJZXtSc_abwZeUHV1Y9vZHlG6BdNiBoALNt8RJTCSVODd48s3, Student, Active"
     * ;
     */
    InputStream is = new ByteArrayInputStream(csv.getBytes());
    BufferedReader bufferReader = new BufferedReader(new InputStreamReader(is));

    // TODO: Add data validation to check if the required fields are set for
    // each different roles
    try {
      CsvReader reader = new CsvReader(is, StandardCharsets.UTF_8);
      reader.readHeaders();
      User user;
      while (reader.readRecord()) {
        // check if user already exists
        if (User.find.where().eq("email", reader.get("Email")).findRowCount() < 1) {
          // signup users
          MySignup signup = new MySignup();
          signup.email = reader.get("Email");
          signup.loginID = reader.get("loginID");
          signup.password = "!zaq123";
          signup.repeatPassword = "!zaq123";
          MyUserServicePlugin userService = new MyUserServicePlugin(Play.application());
          MyUsernamePasswordAuthUser newUser = new MyUsernamePasswordAuthUser(signup);
          userService.save(newUser);

          user = User.findByEmail(signup.email);
          user.studentID = reader.get("StudentID");
          user.firstName = reader.get("FirstName");
          user.lastName = reader.get("LastName");
          user.gender = Gender.valueOf(reader.get("Gender"));
          user.gradeLevel = GradeLevel.valueOf(reader.get("GradeLevel"));
          user.picture = reader.get("picture");
          user.roles = new ArrayList<>();
          user.emailValidated = true;
          user.roles.add(SecurityRole.findByRoleName(Application.USER_ROLE));
          user.roles.add(SecurityRole.findByRoleName(reader.get("Role")));
          user.update();
        }
      }

      return GO_HOME;

    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Restrict(@Group(Application.USER_ROLE))
  public static Result generateParentCode(String id) {
    User user = User.find.ref(id);
    user.GenerateParentCode();
    return GO_HOME;
  }
  
  public static void createTeacher(String email, String loginID, String pw, String firstName, String lastName, Gender gender){
    MySignup signup = new MySignup();
    signup.email = email;
    signup.loginID = loginID;
    signup.password= pw;
    signup.repeatPassword = pw;
    MyUserServicePlugin userService = new MyUserServicePlugin(Play.application());
    MyUsernamePasswordAuthUser newUser = new MyUsernamePasswordAuthUser(signup);
    userService.save(newUser);
          
    User user = User.findByEmail(signup.email);
    user.firstName = firstName;
    user.lastName = lastName;
    user.gender = gender;
    user.emailValidated = true;
    user.roles = new ArrayList<>();
    user.roles.add(SecurityRole.findByRoleName(controllers.Application.USER_ROLE));
    user.roles.add(SecurityRole.findByRoleName(controllers.Application.TEACHER_ROLE));
    user.update();
  }

  public static void createStudent(String email, String loginID, String pw, String firstName, String lastName, Gender gender){
    MySignup signup = new MySignup();
    signup.email = email;
    signup.loginID = loginID;
    signup.password= pw;
    signup.repeatPassword = pw;
    MyUserServicePlugin userService = new MyUserServicePlugin(Play.application());
    MyUsernamePasswordAuthUser newUser = new MyUsernamePasswordAuthUser(signup);
    userService.save(newUser);
          
    User user = User.findByEmail(signup.email);
    user.firstName = firstName;
    user.lastName = lastName;
    user.gender = gender;
    user.emailValidated = true;
    user.roles = new ArrayList<>();
    user.roles.add(SecurityRole.findByRoleName(controllers.Application.USER_ROLE));
    user.roles.add(SecurityRole.findByRoleName(controllers.Application.STUDENT_ROLE));
    user.update();
  }
}
