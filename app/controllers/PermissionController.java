package controllers;

import static play.data.Form.form;
import models.UserPermission;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

public class PermissionController extends Controller{

	/**
	 * Display the 'new lesson element form'.
	 */
	public static Result create() {
		Form<UserPermission> permissionForm = form(UserPermission.class);
		return ok(
				views.html.permission.createForm.render(permissionForm)
				);
	}

	/**
	 * Handle the 'new lesson element form' submission
	 */

	public static Result save() {
		Form<UserPermission> createForm = form(UserPermission.class)
				.bindFromRequest();

		if (createForm.hasErrors()) {
			return badRequest(
					views.html.permission.createForm.render(createForm)
					);
		}
		
		UserPermission permission = new UserPermission();
		permission.value = createForm.get().value;
		permission.save();
		
		flash("success", "Permission " + createForm.get().value
				+ " has been created");
		return ok();
	}
	
}
