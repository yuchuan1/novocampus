package controllers;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Usage:
 * 
 * Map<String, String> myMap = new HashMap<String, String>();
 * System.out.println(new PrettyPrintingMap<String, String>(myMap));
 * 
 */
public class PrettyPrintingMap<K, V> {
    private Map<K, V> map;

    public PrettyPrintingMap(Map<K, V> map) {
        this.map = map;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        Iterator<Entry<K, V>> iter = map.entrySet().iterator();
        while (iter.hasNext()) {
            Entry<K, V> entry = iter.next();
            sb.append(entry.getKey());
            sb.append('=').append('"');
            sb.append(entry.getValue());
            sb.append('"');
            if (iter.hasNext()) {
                //sb.append(',').append(' ');
            		//sb.append("\n");
                sb.append(System.getProperty("line.separator"));	// better
            }
        }
        return sb.toString();

    }
}