package controllers;

import static play.data.Form.form;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import play.data.Form;
import play.data.validation.ValidationError;
import play.mvc.Controller;
import play.mvc.Result;
import utils.BreadCrumb;
import models.SchoolYear;
import models.Variable;

public class SchoolYearController extends Controller {

  /**
   * This result directly redirect to application home.
   */
  public static Result GO_HOME = redirect(routes.SchoolYearController.list());
  public static String nav = "/schoolYears";
  private static String currentSchoolYear = "Current-School-Year";

  /**
   * Handle default path requests, redirect to school years list
   */  
  public static Result index() {
    return GO_HOME;
  }
  
  /**
   * Display the paginated list of school years.
   */
  public static Result list() {
    System.out.println(request().acceptLanguages());

    List<SchoolYear> schoolYears = SchoolYear.find.all();
	  BreadCrumb breadCrumb = new BreadCrumb("School Years", "/schoolYears");
	  breadCrumb.nav = nav;
    return ok(views.html.schoolYear.list.render(schoolYears, breadCrumb));
  }

  /**
   * Display the 'new school year form'.
   */
  public static Result create() {
    BreadCrumb breadCrumb = new BreadCrumb("schoolYear.create.title", routes.SchoolYearController.create().url());
    breadCrumb.nav = nav;
		breadCrumb.Add("School Years", "/schoolYears");
		
    Form<SchoolYear> schoolYearForm = form(SchoolYear.class);
    // TODO: set some default value of created form
    //schoolYearForm.fill(currentSemester);  // assign default value
    return ok(views.html.schoolYear.createForm.render(schoolYearForm, breadCrumb));
  }

  /**
   * Handle the 'new school year form' submission
   */
  public static Result save() {
    Form<SchoolYear> schoolYearForm = form(SchoolYear.class).bindFromRequest();

    List<ValidationError> errors = new ArrayList<ValidationError>();
/*
    if (SchoolYear.find.where().eq("title", schoolYearForm.get().title()).findRowCount() > 0) {
      errors.add(new ValidationError("title", "The title is already existed."));
      schoolYearForm.errors().put("title", errors);
    }
*/
    // TODO: check duplicate startDate and endDate
    
    if (schoolYearForm.hasErrors()) {
      BreadCrumb breadCrumb = new BreadCrumb("New");
      breadCrumb.nav = nav;
      breadCrumb.Add("School Years", "/schoolYears");
      return badRequest(views.html.schoolYear.createForm.render(schoolYearForm, breadCrumb));
    }
    
    try {
      schoolYearForm.get().save();
      flash("success", "school year " + schoolYearForm.get().title() + " has been created");
      return GO_HOME;
	  }
	  catch (javax.persistence.PersistenceException ex) {
	    System.out.println(ex.getClass());
	    System.out.println(ex.getMessage());
      errors.add(new ValidationError("academicYear", "The academicYear and semester set is already existed."));
      schoolYearForm.errors().put("academicYear", errors);
      schoolYearForm.errors().put("semester", errors);
      //System.out.println(new PrettyPrintingMap<String, List<ValidationError>>(schoolYearForm.errors()));
      BreadCrumb breadCrumb = new BreadCrumb("New");
      breadCrumb.nav = nav;
      breadCrumb.Add("School Years", "/schoolYears");
      return badRequest(views.html.schoolYear.createForm.render(schoolYearForm, breadCrumb));
	  }
    catch (Exception ex){
	    System.out.println(ex.getClass());
	    System.out.println(ex.getMessage());
    	throw ex;
    }

  }

  /**
   * Display the 'edit form' of an existing school year.
   * 
   * @param id
   *          Id of the school year to edit
   */
  public static Result edit(String id) {
    SchoolYear schoolYear = SchoolYear.find.byId(id);

    BreadCrumb breadCrumb = new BreadCrumb("Edit");
    breadCrumb.nav = nav;
    breadCrumb.Add("School Years", "/schoolYears");
    breadCrumb.Add(schoolYear.title(), "/schoolYears/"+id);

    Form<SchoolYear> editForm = form(SchoolYear.class).fill(schoolYear);
    return ok(views.html.schoolYear.editForm.render(id, editForm, schoolYear, breadCrumb));
  }

  /**
   * Handle the 'edit form' submission
   * 
   * @param id
   *          Id of the school year to edit
   */
  public static Result update(String id) {
    SchoolYear schoolYear = SchoolYear.find.byId(id);
    Form<SchoolYear> editForm = form(SchoolYear.class).bindFromRequest();
    if (editForm.hasErrors()) {
      BreadCrumb breadCrumb = new BreadCrumb("Edit");
      breadCrumb.nav = nav;
      breadCrumb.Add("School Years", "/schoolYears");
      breadCrumb.Add(schoolYear.title(), "/schoolYears/"+id);
      return badRequest(views.html.schoolYear.editForm.render(id, editForm, schoolYear, breadCrumb));
    }
    System.out.println("==============================");
    System.out.println(editForm.get().description);
    System.out.println("==============================");
    editForm.get().update((Object) id);
    flash("success", "School year " + editForm.get().title() + " has been updated");
    return GO_HOME;
  }

  /**
   * Handle school year remove form
   */
  public static Result remove(String id) {
    SchoolYear schoolYear = SchoolYear.find.byId(id);
    String destination = nav;
    BreadCrumb breadCrumb = new BreadCrumb("Delete " + schoolYear.title());
    breadCrumb.nav = nav;
    breadCrumb.Add("School Years", "/schoolYears");
    breadCrumb.destination = destination;
    return ok(views.html.tags.removeForm.render(schoolYear.title(), routes.SchoolYearController.delete(id).url(), breadCrumb));
  }

  /**
   * Handle school year deletion
   */
  public static Result delete(String id) {
    String destination = nav;
    SchoolYear schoolYear = SchoolYear.find.ref(id);
    String title = schoolYear.title();
    if (schoolYear.id.equals(getCurrentSchoolYearID())){
      // TODO: Delete Variable
      // Variable variable = Variable.find.where().eq("name",currentSchoolYear).findUnique();
      // variable.delete(); // This doesn't work.
    }
    schoolYear.delete();
    flash("success", "School year ["+ title +"] has been deleted");
    return (destination=="") ? GO_HOME : redirect(destination);
  }

  /**
   * Handle set Current school year
   */
  public static Result setCurrent() {
    BreadCrumb breadCrumb = new BreadCrumb("Set Current School Year");
    breadCrumb.nav = nav;
    breadCrumb.Add("School Years", "/schoolYears");
    
    Map<String, String> currentSemester = new HashMap<String, String>();
    currentSemester.put("id", getCurrentSchoolYearID());
    Form<SchoolYear> currentForm = form(SchoolYear.class).bind(currentSemester);
    return ok(views.html.schoolYear.setCurrentForm.render(breadCrumb, currentForm));
  }

  /**
   * Handle save Current school year
   */
  public static Result saveCurrent() {
  	Form<SchoolYear> currentForm = Form.form(SchoolYear.class).bindFromRequest();
  	
  	// Check validation
    List<ValidationError> errors = new ArrayList<ValidationError>();
    if (currentForm.data().get("id")=="" || currentForm.data().get("id").isEmpty()) {
      errors.add(new ValidationError("id", "You must choose one!"));
      currentForm.errors().put("id", errors);
    }    
    if (errors.size() > 0) {
      BreadCrumb breadCrumb = new BreadCrumb("Set Current School Year");
      breadCrumb.nav = nav;
      breadCrumb.Add("School Years", "/schoolYears");
      return badRequest(views.html.schoolYear.setCurrentForm.render(breadCrumb, currentForm));
    }

  	Variable variable = Variable.find.where().eq("name",currentSchoolYear).findUnique();
  	if (variable==null){
  		variable = new Variable(currentSchoolYear, currentForm.data().get("id"));  		
  	}
  	else{
  		variable.value = currentForm.data().get("id");
  	}
  	variable.save();
    return redirect("/schoolYears");
  }
  
  public static SchoolYear getCurrentSchoolYear(){
  	SchoolYear schoolYear = null;
  	Variable variable = Variable.find.where().eq("name",currentSchoolYear).findUnique();
  	if (variable!=null){
  		schoolYear = SchoolYear.find.where().eq("id", variable.value).findUnique();
  	}
  	return schoolYear;
  }

  public static String getCurrentSchoolYearID(){
  	SchoolYear schoolYear = getCurrentSchoolYear();
  	return (schoolYear==null) ? "" : schoolYear.id;
  }

}