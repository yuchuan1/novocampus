package controllers;


import java.io.StringWriter;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONWriter;

import models.Message;
import models.User;

import com.fasterxml.jackson.databind.JsonNode;

import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import scala.Console;

public class MessageController extends Controller {
	
	 public static Result send() {
		 Date sent = new Date();
		 JsonNode json = request().body().asJson();
		 if(json == null) {
			    return badRequest("Expecting Json data");
			  } else {
				  String from = json.findPath("from").textValue();
				  String to = json.findPath("to").textValue();
				  String body = json.findPath("body").textValue();
				  
				  if(from == null) {
				      return badRequest("Missing parameter [from]");
				    } else if(to == null) {
					      return badRequest("Missing parameter [to]");
					}
				    else
				    {
				    	try
				    	{
				    	Message msg = new Message();
				    	msg.from_uid = from;
				    	msg.to_uid = to;
				    	msg.body = body.trim();
				    	msg.sent = sent;
				    	msg.save();
				    	}
				    	catch(Exception ex)
				    	{
				    		Console.out().append(ex.getMessage());
				    	}
				    	
				      return ok("message sent from: " + from + "\nmessage sent to: " + to + "\nBody: " + body + "\nat " + sent.toString());
				    }
				  
				
			  }

		  }

	 public static Result getMessages()
	 {
		 
		 String from ="JeffScott@novocampus.com.tw";
		 List<Message> messages =  Message.find.where().ieq("from_uid", from.trim()).orderBy("sent").findList();
		 
		 StringWriter writer = new StringWriter();
		 JSONWriter jsonWriter = new JSONWriter(writer);
		 
		 try {
			jsonWriter.array();
		 
		 for(Iterator<Message> i = messages.iterator(); i.hasNext(); )
		 {
			 Message msg = i.next();
			 jsonWriter.object();
			 jsonWriter.key("from_uid");
			 jsonWriter.value(msg.from_uid);
			 
			 jsonWriter.key("to_uid");
			 jsonWriter.value(msg.to_uid);
			 
			 jsonWriter.key("body");
			 jsonWriter.value(msg.body);
			 
			 jsonWriter.key("sent");
			 jsonWriter.value(msg.sent.toString());
			 
			 jsonWriter.endObject();
			 
		 }
		 
		 jsonWriter.endArray();
			 
		 } catch (JSONException e) {

				e.printStackTrace();
				return Results.badRequest(e.getMessage());
		}
		 
		return ok(writer.toString());		 
	 }
}
