package controllers;

import static play.data.Form.form;

import java.util.List;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import com.avaje.ebean.Page;
import com.google.common.base.Strings;

import be.objectify.deadbolt.java.actions.Restrict;
import play.data.Form;
import play.data.validation.ValidationError;
import play.mvc.Controller;
import play.mvc.Result;
import utils.BreadCrumb;
import utils.PageHelper;
import models.Group;
import models.Membership;
import models.SchoolYear;
import models.SecurityRole;
import models.User;
import models.enums.GradeLevel;
import views.formdata.GroupAddMemberForm;
import views.formdata.GroupFormData;
import views.formdata.UserSearchFormData;


public class GroupController extends Controller {

  /**
   * This result directly redirect to application home.
   */
  public static Result GO_HOME = redirect(routes.GroupController.list(0, "name", "asc", ""));
  public static String nav = "/groups";

  /**
   * Handle default path requests, redirect to group list
   */
  public static Result index() {
    return GO_HOME;
  }

  /**
   * Display the paginated list of groups.
   * 
   * @param page
   *          Current page number (starts from 0)
   * @param sortBy
   *          Column to be sorted
   * @param order
   *          Sort order (either asc or desc)
   * @param filter
   *          Filter applied on group titles
   */
  public static Result list(int page, String sortBy, String order, String filter) {
    Page<Group> currentPage = Group.page(page, 10, sortBy, order, filter);
    PageHelper pageHelper = new PageHelper("Group", sortBy, order, filter, currentPage.getPageIndex(), currentPage.getTotalPageCount(), currentPage.getTotalRowCount(), currentPage.hasPrev(), currentPage.hasNext());
    BreadCrumb breadCrumb = new BreadCrumb("group.list.title", nav);
    breadCrumb.nav = nav;
    return ok(views.html.group.list.render(currentPage, pageHelper, breadCrumb));
  }

  /**
   * Display the 'new group form'.
   */
  public static Result create() {
    BreadCrumb breadCrumb = new BreadCrumb("group.create.title", routes.GroupController.create().url());
    breadCrumb.nav = nav;
    breadCrumb.Add("group.list.title", nav);

    // User logged = Application.getLocalUser();

    GroupFormData groupData = new GroupFormData();
    groupData.gradeLevel = GradeLevel.Five;
    groupData.schoolYear = SchoolYearController.getCurrentSchoolYearID();
    groupData.roomTeacher = Application.getLocalUser(session()).id;

    Form<GroupFormData> groupForm = form(GroupFormData.class);
    groupForm = groupForm.fill(groupData);
    return ok(views.html.group.createForm.render(groupForm, breadCrumb));
  }

  /**
   * Handle the 'new group form' submission
   */
  public static Result save() {
    String destination = request().getQueryString("destination").toString();
    BreadCrumb breadCrumb = new BreadCrumb("New");
    breadCrumb.nav = nav;
    breadCrumb.destination = destination;
    breadCrumb.Add("group.list.title", nav);

    Form<GroupFormData> groupForm = form(GroupFormData.class).bindFromRequest();
    if (groupForm.hasErrors()) {
      System.out.println("Group save...............");
      System.out.println(new PrettyPrintingMap<String, List<ValidationError>>(groupForm.errors()));
      return badRequest(views.html.group.createForm.render(groupForm, breadCrumb));
    }

    Group group = new Group();
    group.name = groupForm.get().name;
    group.shortName = groupForm.get().shortName;
    group.gradeLevel = groupForm.get().gradeLevel;
    group.schoolYear = SchoolYear.find.byId(groupForm.get().schoolYear);
    // System.out.println(groupForm.get().roomTeacher);
    group.roomTeacher = User.find.byId(groupForm.get().roomTeacher);
    group.save();

    flash(Application.FLASH_SUCCESS_KEY, "Group " + groupForm.get().name + " has been created");
    return (destination == "") ? GO_HOME : redirect(destination);
  }

  /**
   * Display the 'edit form' of an existing group.
   * 
   * @param id
   *          Id of the group to edit
   */
  public static Result edit(String id) {
    GroupFormData groupformData = Group.makeGroupFormData(id);
    Form<GroupFormData> editForm = form(GroupFormData.class).fill(groupformData);
    String destination = (Strings.isNullOrEmpty(request().getQueryString("destination"))) ? routes.GroupController.view(id).url() : request().getQueryString("destination").toString();

    BreadCrumb breadCrumb = new BreadCrumb("Edit");
    breadCrumb.nav = nav;
    breadCrumb.destination = destination;
    breadCrumb.Add("group.list.title", nav);
    breadCrumb.Add(groupformData.name, routes.GroupController.view(id).url());

    return ok(views.html.group.editForm.render(id, editForm, breadCrumb));
  }

  /**
   * Handle the 'edit form' submission
   * 
   * @param id
   *          Id of the group to edit
   */
  public static Result update(String id) {
    //String destination = (Strings.isNullOrEmpty(request().getQueryString("destination"))) ? routes.GroupController.view(id).url() : request().getQueryString("destination").toString();
    String destination = (Strings.isNullOrEmpty(request().getQueryString("destination"))) ? routes.GroupController.addMember(id).url() : request().getQueryString("destination").toString();

    BreadCrumb breadCrumb = new BreadCrumb("Edit");
    breadCrumb.nav = nav;
    breadCrumb.destination = destination;
    breadCrumb.Add("group.list.title", nav);

    Form<GroupFormData> editForm = form(GroupFormData.class).bindFromRequest();
    if (editForm.hasErrors()) {
      return badRequest(views.html.group.editForm.render(id, editForm, breadCrumb));
    }

    Group group = Group.makeInstance(editForm.get());
    group.update();

    flash(Application.FLASH_SUCCESS_KEY, "Group " + editForm.get().name + " has been updated");
    return (destination == "") ? GO_HOME : redirect(destination);
  }

  /**
   * Handle group remove form
   */
  public static Result remove(String id) {
    Group group = Group.find.byId(id);
    String destination = (Strings.isNullOrEmpty(request().getQueryString("destination"))) ? nav : request().getQueryString("destination").toString();
    
    BreadCrumb breadCrumb = new BreadCrumb("Delete");
    breadCrumb.nav = nav;
    breadCrumb.destination = destination;
    breadCrumb.Add("group.list.title", nav);
    breadCrumb.Add(group.name, routes.GroupController.view(id).url());
    
    return ok(views.html.tags.removeForm.render(group.name, routes.GroupController.delete(id).url(), breadCrumb));
  }

  /**
   * Handle group deletion
   */
  public static Result delete(String id) {
    String destination = (Strings.isNullOrEmpty(request().getQueryString("destination"))) ? nav : request().getQueryString("destination").toString();
    Group.find.ref(id).delete();
    flash(Application.FLASH_SUCCESS_KEY, "Group has been deleted");
    return (destination == "") ? GO_HOME : redirect(destination);
  }

  /**
   * Handle group detail page
   */
  public static Result view(String id) {
    Group group = Group.find.byId(id);
    BreadCrumb breadCrumb = new BreadCrumb(group.name, routes.GroupController.view(group.id).url());
    breadCrumb.nav = nav;
    breadCrumb.Add("group.list.title", nav);
    return ok(views.html.group.view.render(group, breadCrumb));
  }

  /**
   * Handle group code generation
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(Application.USER_ROLE))
  public static Result generateGroupCode(String id) {
    Group group = models.Group.find.ref(id);
    group.generateGroupCode();
    flash(Application.FLASH_SUCCESS_KEY, "Group Code has been genertaed.");
    return redirect(routes.GroupController.view(group.id));
  }

  /**
   * Handle group code deletion
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(Application.USER_ROLE))
  public static Result removeGroupCode(String id) {
    Group group = models.Group.find.ref(id);
    group.removeGroupCode();
    flash(Application.FLASH_SUCCESS_KEY, "Group Code has been removed.");
    return redirect(routes.GroupController.view(group.id));
  }

  /**
   * Handle the 'add group member form'
   */
  public static Result addMember(String id) {
    Group group = Group.find.byId(id);
  
    List<User> users = null;
    Form<UserSearchFormData> searchForm = Form.form(UserSearchFormData.class);
    Form<GroupAddMemberForm> memberForm = form(GroupAddMemberForm.class);
  
    BreadCrumb breadCrumb = new BreadCrumb("Add Members");
    breadCrumb.nav = nav;
    breadCrumb.Add("group.list.title", nav);
    breadCrumb.Add(group.name, routes.GroupController.view(group.id).url());
    
    return ok(views.html.group.addStudentForm.render(group, breadCrumb, searchForm, users, memberForm));
  }
  /**
   * Handle the 'add group member form' submission
   */
  public static Result saveMember(String id) {
    Group group = Group.find.byId(id);

    Form<UserSearchFormData> searchForm = Form.form(UserSearchFormData.class);
    Form<GroupAddMemberForm> memberForm = form(GroupAddMemberForm.class).bindFromRequest();

    if (memberForm.hasErrors()) {
      flash(Application.FLASH_ERROR_KEY, "Something wrong when saveMember ...");
      BreadCrumb breadCrumb = new BreadCrumb("Add Students");
      breadCrumb.nav = nav;
      breadCrumb.Add("group.list.title", nav);
      breadCrumb.Add(group.name, routes.GroupController.view(group.id).url());
      List<User> users = User.find.all();
      return badRequest(views.html.group.addStudentForm.render(group, breadCrumb, searchForm, users, memberForm));
    }

    int i = 0;
    for (String uid : memberForm.get().uids) {
      if (Membership.find.where().eq("user_id", uid).eq("group_id", id).findRowCount() == 0) {
        if (group.addMember(User.find.ref(uid))) i++;
      }
    }
    
    flash(Application.FLASH_MESSAGE_KEY, i + " student" + ((i > 1)? "s are" : " is") + " added.");
    return redirect(routes.GroupController.view(group.id));
  }

  /**
   * Handle 'search student'
   */
  public static Result searchStudent(String id) {
    Group group = Group.find.byId(id);
  
    List<User> users = null;
    Form<UserSearchFormData> searchForm = Form.form(UserSearchFormData.class);
    Form<GroupAddMemberForm> memberForm = form(GroupAddMemberForm.class);
  
    BreadCrumb breadCrumb = new BreadCrumb("Search Students");
    breadCrumb.nav = nav;
    breadCrumb.Add("group.list.title", nav);
    breadCrumb.Add(group.name, routes.GroupController.view(group.id).url());
    
    return ok(views.html.group.searchStudentForm.render(group, breadCrumb, searchForm, users, memberForm));
  }

  /**
   * Handle the 'search student' submission
   */
  public static Result searchUser(String id) {
    Group group = Group.find.byId(id);

    BreadCrumb breadCrumb = new BreadCrumb("Add Students");
    breadCrumb.nav = nav;
    breadCrumb.Add("group.list.title", nav);
    breadCrumb.Add(group.name, routes.GroupController.view(group.id).url());

    Form<UserSearchFormData> searchForm = Form.form(UserSearchFormData.class).bindFromRequest();
    
    SecurityRole studentRole = SecurityRole.getStudentRole();
    //System.out.println("Search User.....");
    //System.out.println(studentRole.id);

    List<User> users = null;
    if (!Strings.isNullOrEmpty(searchForm.get().f) && !Strings.isNullOrEmpty(searchForm.get().gradeLevel)) {
    	users = User.find.where()
							.or(
								Expr.like("name", "%" + searchForm.get().f + "%"),
								Expr.like("studentID", searchForm.get().f + "%"))
							.eq("gradeLevel", GradeLevel.valueOf(searchForm.get().gradeLevel))
							.eq("roles.id", studentRole.id)
							.findList();
    } else if (!Strings.isNullOrEmpty(searchForm.get().f)) {
    	users = User.find.where()
							.or(
								Expr.like("name", "%" + searchForm.get().f + "%"),
								Expr.like("studentID", searchForm.get().f + "%"))
	              .eq("roles.id", studentRole.id)
							.findList();
    } else if (!Strings.isNullOrEmpty(searchForm.get().gradeLevel)) {
    	users = User.find.where()
							.eq("gradeLevel", GradeLevel.valueOf(searchForm.get().gradeLevel))
              .eq("roles.id", studentRole.id)
							.findList();
    } else {
      users = User.find.all();
    }
    
    Form<GroupAddMemberForm> memberForm = form(GroupAddMemberForm.class);

    if (users.size()==0)
      flash(Application.FLASH_MESSAGE_KEY, "No data found.");
    else
      flash(Application.FLASH_MESSAGE_KEY, users.size() + " student" + (users.size()==1 ? " is" : "s are") + " found.");
      
    return ok(views.html.group.searchStudentForm.render(group, breadCrumb, searchForm, users, memberForm));
  }

  /**
   * Handle the 'remove group member form'
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(Application.USER_ROLE))
  public static Result deleteStudent(String id) {
    Group group = Group.find.byId(id);

    //List<User> users = User.find.where().eq("memberships.group", group.id).findList();
    List<User> users = User.find.where().eq("memberships.group.id", group.id).findList();
    //.eq("roles.id", studentRole.id)
/*
    List<Membership> memberships = Membership.find.where().eq("group_id", id).findList();
    for (Membership membership : memberships) {
      System.out.println("UID = " + membership.user.id);
      users.add(membership.user);
    }
*/
    Form<GroupAddMemberForm> memberForm = form(GroupAddMemberForm.class);

    BreadCrumb breadCrumb = new BreadCrumb("Delete Students", routes.GroupController.deleteStudent(group.id).url());
    breadCrumb.nav = nav;
    breadCrumb.Add("group.list.title", nav);
    breadCrumb.Add(group.name, routes.GroupController.view(group.id).url());
    
    return ok(views.html.group.deleteStudentForm.render(group, breadCrumb, users, memberForm));
  }

  /**
   * Handle the 'remove group member form' submission
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(Application.USER_ROLE))
  public static Result deleteMember(String id) {
    Group group = Group.find.byId(id);
    Form<GroupAddMemberForm> memberForm = form(GroupAddMemberForm.class).bindFromRequest();
    
    System.out.println("Delete member.....");
    int i = 0;
    for (String uid : memberForm.get().uids) {
      System.out.println("UID = " + uid);
      if (group.deleteMember(uid)) i++;
    }
    
    flash(Application.FLASH_MESSAGE_KEY, i + " student" + ((i > 1)? "s are" : " is") + " deleted.");
/*
    BreadCrumb breadCrumb = new BreadCrumb("Delete Students", routes.GroupController.deleteStudent(group.id).url());
    breadCrumb.nav = nav;
    breadCrumb.Add("group.list.title", nav);
    breadCrumb.Add(group.name, routes.GroupController.view(group.id).url());
*/
    return redirect(routes.GroupController.view(group.id));
  }

}
