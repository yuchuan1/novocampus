package controllers;

import static play.data.Form.form;

import com.google.common.base.Strings;

import models.Course;
import models.Group;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import utils.BreadCrumb;
import views.formdata.CourseFormData;

public class CourseController extends Controller {

	/**
	 * Display the paginated list of courses.
	 * 
	 * @param page
	 *            Current page number (starts from 0)
	 * @param sortBy
	 *            Column to be sorted
	 * @param order
	 *            Sort order (either asc or desc)
	 * @param filter
	 *            Filter applied on courses
	 */
	public static Result list(int page, String sortBy, String order,
			String filter) {
		
		BreadCrumb breadCrumb = new BreadCrumb("Courses");
		breadCrumb.Add("Home", "/");
		
		return ok(
				views.html.course.list.render(Course.page(page, 10, sortBy, order, filter), sortBy, order, filter, breadCrumb)
		);
	}
	
	/**
	 * Display the 'new course form'.
	 */
	public static Result create() {
		BreadCrumb breadCrumb = new BreadCrumb("New");
		breadCrumb.Add("Home", "/");
		breadCrumb.Add("Courses", "/courses");
		
		Form<CourseFormData> courseForm = form(CourseFormData.class);
		return ok(
				views.html.course.createForm.render(courseForm, breadCrumb)
				);
	}
	
	/**
	 * Handle the 'new course form' submission
	 */

	public static Result save() {
		BreadCrumb breadCrumb = new BreadCrumb("New");
		breadCrumb.Add("Home", "/");
		breadCrumb.Add("Courses", "/courses");
		
		Form<CourseFormData> courseForm = form(CourseFormData.class).bindFromRequest();
		if (courseForm.hasErrors()) {
			return badRequest(
					views.html.course.createForm.render(courseForm, breadCrumb)
					);
		}
		else
		{		
			Course course = new Course();
			course.body = courseForm.get().body;
			course.courseModule = courseForm.get().courseModule;
			course.coverImageUrl = courseForm.get().coverImageUrl;
			course.enabled = courseForm.get().enabled;
			
			if(!Strings.isNullOrEmpty(courseForm.get().group))
				course.group = Group.find.where().eq("title", courseForm.get().group).findUnique();
			
			course.title = courseForm.get().title;
			
			course.save();
	
		
			flash("success", "Course " + courseForm.get().title
					+ " has been created");
		//	return GO_HOME;
			return ok();
		}
	}
	
	/**
	 * Display the 'edit form' of a existing course.
	 * 
	 * @param id
	 *            Id of the course to edit
	 */
	public static Result edit(String id) {
		BreadCrumb breadCrumb = new BreadCrumb("Edit");
		breadCrumb.Add("Home", "/");
		breadCrumb.Add("Courses", "/courses");
		
		CourseFormData courseData = Course.makeCourseFormData(id);
		Form<CourseFormData> editForm = form(CourseFormData.class).fill(courseData);
		
		return ok(
				views.html.course.editForm.render(id, editForm, breadCrumb)
				);
	}


	/**
	 * Handle the 'edit form' submission
	 * 
	 * @param id
	 *            Id of the course to edit
	 */
	public static Result update(String id) {
		BreadCrumb breadCrumb = new BreadCrumb("Edit");
		breadCrumb.Add("Home", "/");
		breadCrumb.Add("Courses", "/courses");
		
		//CourseFormData courseData = Course.makeCourseFormData(id);
		Form<CourseFormData> courseForm = form(CourseFormData.class).bindFromRequest();
		if (courseForm.hasErrors()) {
			return badRequest(
					views.html.course.editForm.render(id, courseForm, breadCrumb)
					);
		}
		else
        {
			
		//lessonForm.get().update((Object)id);
		flash("success", "Lesson " + courseForm.get().title
				+ " has been updated");
        }
		
		return ok();
	}
	
	/**
	 * Handle course deletion
	 */
	public static Result delete(String id) {
		Course.find.ref(id).delete();
		flash("success", "Course has been deleted");
		return ok();

	}
}
