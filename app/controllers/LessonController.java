package controllers;

import static play.data.Form.form;

import com.avaje.ebean.Ebean;

import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

import utils.BreadCrumb;

import models.Lesson;
import models.LessonElement;
import models.Subject;

import views.formdata.LessonFormData;


public class LessonController extends Controller {

  /**
   * This result directly redirect to application home.
   */
  public static Result GO_HOME = redirect(routes.LessonController.list(0, "title", "asc", ""));

  /**
   * Handle default path requests, redirect to lesson elements list
   */
  public static Result index() {
    return GO_HOME;
  }

  /**
   * Display the paginated list of lessons.
   * 
   * @param page
   *          Current page number (starts from 0)
   * @param sortBy
   *          Column to be sorted
   * @param order
   *          Sort order (either asc or desc)
   * @param filter
   *          Filter applied on lesson titles
   */
  public static Result list(int page, String sortBy, String order, String filter) {

    BreadCrumb breadCrumb = new BreadCrumb("Lessons");
    breadCrumb.Add("Home", "/");

    return ok(views.html.lesson.list.render(Lesson.page(page, 10, sortBy, order, filter), sortBy, order, filter, breadCrumb)

    );
  }

  /**
   * Display the 'new lesson form'.
   */
  public static Result create() {
    BreadCrumb breadCrumb = new BreadCrumb("New");
    breadCrumb.Add("Home", "/");
    breadCrumb.Add("Lessons", "/lessons");

    Form<LessonFormData> lessonForm = form(LessonFormData.class);
    return ok(views.html.lesson.createForm.render(lessonForm, LessonElement.makeLessonElementMap(), breadCrumb));
  }

  /**
   * Handle the 'new lesson form' submission
   */

  public static Result save() {
    BreadCrumb breadCrumb = new BreadCrumb("New");
    breadCrumb.Add("Home", "/");
    breadCrumb.Add("Lessons", "/lessons");

    Form<LessonFormData> lessonForm = form(LessonFormData.class).bindFromRequest();
    if (lessonForm.hasErrors()) {
      return badRequest(views.html.lesson.createForm.render(lessonForm, LessonElement.makeLessonElementMap(), breadCrumb));
    }

    Lesson lesson = new Lesson();
    lesson.body = lessonForm.get().body;
    lesson.gradeLevel = lessonForm.get().gradeLevel;
    lesson.image_url = lessonForm.get().image_url;
    lesson.share = lessonForm.get().share;
    lesson.subject = Subject.find.byId(lessonForm.get().subject);
    lesson.title = lessonForm.get().title;
    lesson.lessonElements.clear();

    for (String lessonElement : lessonForm.get().lessonElements) {
      lesson.lessonElements.add(LessonElement.find.where().eq("title", lessonElement).findUnique());
    }

    lesson.save();

    flash("success", "Lesson " + lessonForm.get().title + " has been created");
    return GO_HOME;

  }

  /**
   * Display the 'edit form' of a existing lesson.
   * 
   * @param id
   *          Id of the lesson element to edit
   */
  public static Result edit(String id) {

    BreadCrumb breadCrumb = new BreadCrumb("Edit");
    breadCrumb.Add("Home", "/");
    breadCrumb.Add("Lessons", "/lessons");

    LessonFormData lessonData = Lesson.makeLessonFormData(id);
    Form<LessonFormData> editForm = form(LessonFormData.class).fill(lessonData);

    return ok(views.html.lesson.editForm.render(id, editForm, LessonElement.makeLessonElementMap(lessonData), breadCrumb));
  }

  /**
   * Handle the 'edit form' submission
   * 
   * @param id
   *          Id of the lesson to edit
   */
  public static Result update(String id) {

    BreadCrumb breadCrumb = new BreadCrumb("Edit");
    breadCrumb.Add("Home", "/");
    breadCrumb.Add("Lessons", "/lessons");

    LessonFormData lessonData = Lesson.makeLessonFormData(id);
    Form<LessonFormData> lessonForm = form(LessonFormData.class).bindFromRequest();
    if (lessonForm.hasErrors()) {
      return badRequest(views.html.lesson.editForm.render(id, lessonForm, LessonElement.makeLessonElementMap(lessonData), breadCrumb));
    } else {
      // Convert the formData into a model instance.
      Lesson lesson = Lesson.makeInstance(lessonForm.get());

      lesson.saveManyToManyAssociations("lessonElements");
      lesson.update();
      /*
       * SqlUpdate update =
       * Ebean.createSqlUpdate("UPDATE lesson SET body=:body,  WHERE id=:id")
       * .setParameter("age", 26) .setParameter("id", 100500); int rows =
       * update.execute();
       */

      // lessonForm.get().update((Object)id);
      flash("success", "Lesson " + lessonForm.get().title + " has been updated");
    }

    return GO_HOME;

  }

  /**
   * Handle lesson deletion
   */
  public static Result delete(String id) {
    Lesson.find.ref(id).delete();
    flash("success", "Lesson has been deleted");
    return GO_HOME;

  }
}
