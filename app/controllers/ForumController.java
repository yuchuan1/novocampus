package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;

import models.Forum;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

public class ForumController extends Controller{
	public static Result create(String name)
	{
		Forum forum = new Forum();
		forum.name = name;
		forum.save();
		
		ObjectNode result = Json.newObject();
		result.put("status", "200");
		result.put("message", "Forum created!");
		
		return ok(result);
		
	}
}
