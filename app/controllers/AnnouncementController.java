package controllers;

import static play.data.Form.form;

import com.avaje.ebean.Page;

import models.Announcement;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import utils.BreadCrumb;
import utils.PageHelper;

public class AnnouncementController extends Controller {

  /**
   * This result directly redirect to application home.
   */
  public static Result GO_HOME = redirect(routes.AnnouncementController.list(0, "title", "asc", ""));

  /**
   * Handle default path requests, redirect to Announcement list
   */
  public static Result index() {
    return GO_HOME;
  }

  /**
   * Display the paginated list of Announcements.
   * 
   * @param page
   *          Current page number (starts from 0)
   * @param sortBy
   *          Column to be sorted
   * @param order
   *          Sort order (either asc or desc)
   * @param filter
   *          Filter applied on Announcement titles
   */
  public static Result list(int page, String sortBy, String order, String filter) {
    Page<Announcement> currentPage = Announcement.page(page, 10, sortBy, order, filter);
    PageHelper pageHelper = new PageHelper("Announcement", sortBy, order, filter, currentPage.getPageIndex(), currentPage.getTotalPageCount(), currentPage.getTotalRowCount(), currentPage.hasPrev(), currentPage.hasNext());
    BreadCrumb breadCrumb = new BreadCrumb("Announcements");
    return ok(views.html.announcement.list.render(currentPage, pageHelper, breadCrumb));
  }

  /**
   * Display the 'new announcement form'.
   */
  public static Result create() {
    BreadCrumb breadCrumb = new BreadCrumb("New");
    breadCrumb.Add("Announcements", "/announcements");

    Form<Announcement> announcementForm = form(Announcement.class);
    return ok(views.html.announcement.createForm.render(announcementForm, breadCrumb));
  }

  /**
   * Handle the 'new announcement form' submission
   */
  public static Result save() {
    BreadCrumb breadCrumb = new BreadCrumb("Add an announcement");
    breadCrumb.Add("Announcements", "/announcements");

    Form<Announcement> announcementForm = form(Announcement.class).bindFromRequest();
    if (announcementForm.hasErrors()) {
      return badRequest(views.html.announcement.createForm.render(announcementForm, breadCrumb));
    }
    announcementForm.get().save();
    flash("success", "Announcement " + announcementForm.get().title + " has been created");
    return GO_HOME;
  }

  /**
   * Display the 'edit form' of a existing Announcement.
   * 
   * @param id
   *          Id of the Announcement to edit
   */
  public static Result edit(String id) {
    BreadCrumb breadCrumb = new BreadCrumb("Edit");
    breadCrumb.Add("Announcements", "/announcements");
    Form<Announcement> editForm = form(Announcement.class).fill(Announcement.find.byId(id));
    return ok(views.html.announcement.editForm.render(id, editForm, breadCrumb));
  }

  /**
   * Handle the 'edit form' submission
   * 
   * @param id
   *          Id of the announcement to edit
   */
  public static Result update(String id) {
    BreadCrumb breadCrumb = new BreadCrumb("Edit");
    breadCrumb.Add("Announcements", "/announcements");
    Form<Announcement> announcementForm = form(Announcement.class).bindFromRequest();
    if (announcementForm.hasErrors()) {
      return badRequest(views.html.announcement.editForm.render(id, announcementForm, breadCrumb));
    }
    announcementForm.get().update((Object) id);
    flash("success", "Announcement " + announcementForm.get().title + " has been updated");
    return GO_HOME;
  }

  /**
   * Handle announcement deletion
   */
  public static Result delete(String id) {
    Announcement.find.ref(id).delete();
    flash("success", "Lesson Element has been deleted");
    return GO_HOME;
  }
}
