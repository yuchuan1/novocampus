package controllers;

import models.CourseModule;
import models.Lesson;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import utils.BreadCrumb;
import views.formdata.CourseModuleFormData;

import static play.data.Form.form;

public class CourseModuleController extends Controller{
	
	/**
	 * This result directly redirect to application home.
	 */
	public static Result GO_HOME = redirect(routes.CourseModuleController.list(0,
			"title", "asc", ""));

	/**
	 * Handle default path requests, redirect to course module list
	 */
	public static Result index() {
		return GO_HOME;
	}
	
	/**
	 * Display the paginated list of Course Module.
	 * 
	 * @param page
	 *            Current page number (starts from 0)
	 * @param sortBy
	 *            Column to be sorted
	 * @param order
	 *            Sort order (either asc or desc)
	 * @param filter
	 *            Filter applied on Course Module titles
	 */
	public static Result list(int page, String sortBy, String order,
			String filter) {
		BreadCrumb breadCrumb = new BreadCrumb("Course Modules");
		breadCrumb.Add("Home", "/");
		return ok(
				views.html.courseModule.list.render(CourseModule.page(page, 10, sortBy, order, filter), sortBy,	order, filter, breadCrumb)
				);
	}

	/**
	 * Display the 'new course module form'.
	 */
	public static Result create() {
		BreadCrumb breadCrumb = new BreadCrumb("New");
		breadCrumb.Add("Home", "/");
		breadCrumb.Add("Course Modules", "/courseModules");
		
		Form<CourseModuleFormData> courseModuleForm = form(CourseModuleFormData.class);
		return ok(
				views.html.courseModule.createForm.render(courseModuleForm, Lesson.makeLessonMap(), breadCrumb)
				);
	}

	/**
	 * Handle the 'new course module form' submission
	 */

	public static Result save() {
		BreadCrumb breadCrumb = new BreadCrumb("New");
		breadCrumb.Add("Home", "/");
		breadCrumb.Add("Course Modules", "/courseModules");
		
		Form<CourseModuleFormData> moduleForm = form(CourseModuleFormData.class).bindFromRequest();
		if (moduleForm.hasErrors()) {
			return badRequest(
					views.html.courseModule.createForm.render(moduleForm, Lesson.makeLessonMap(), breadCrumb)
					);
		}
		
		CourseModule module = new CourseModule();
		module.body = moduleForm.get().body;
		module.courseModuleCode = moduleForm.get().courseModuleCode;
		module.coverImageUrl = moduleForm.get().coverImageUrl;
		module.shared = moduleForm.get().shared;
		module.title = moduleForm.get().title;
		
		for (String lesson : moduleForm.get().lessons) {
			module.lessons.add(Lesson.find.where().eq("title", lesson).findUnique());
	    }
		module.save();
		flash("success", "Course Module " + moduleForm.get().title
				+ " has been created");
		return GO_HOME;
	}
	
	 /**
     * Display the 'edit form' of a existing course module.
     *
     * @param id Id of the CourseModule to edit
     */
    public static Result edit(String id) {
    	BreadCrumb breadCrumb = new BreadCrumb("Edit");
		breadCrumb.Add("Home", "/");
		breadCrumb.Add("Course Modules", "/courseModules");
    	CourseModuleFormData courseModuleFormData = CourseModule.makeCourseModuleFormData(id);    	
        Form<CourseModuleFormData> editForm = form(CourseModuleFormData.class).fill(courseModuleFormData);
        
        return ok(
           views.html.courseModule.editForm.render(id, editForm, Lesson.makeLessonMap(courseModuleFormData), breadCrumb)
        );
    }
    
    /**
     * Handle the 'edit form' submission 
     *
     * @param id Id of the course module to edit
     */
    public static Result update(String id) {
    	BreadCrumb breadCrumb = new BreadCrumb("Edit");
		breadCrumb.Add("Home", "/");
		breadCrumb.Add("Course Modules", "/courseModules");
		
    	CourseModuleFormData courseModuleFormData = CourseModule.makeCourseModuleFormData(id);
        Form<CourseModuleFormData> editForm = form(CourseModuleFormData.class).bindFromRequest();
        if(editForm.hasErrors()) {
            return badRequest(
            		views.html.courseModule.editForm.render(id, editForm, Lesson.makeLessonMap(courseModuleFormData), breadCrumb)
            		);
        }
        else
        {
        	// Convert the formData into a model instance.
        	CourseModule module = CourseModule.makeInstance(editForm.get());
        	module.saveManyToManyAssociations("lessons");
        	module.update();
            
	        //editForm.get().update((Object)id);
	        flash("success", "Course module " + editForm.get().title + " has been updated");
	        return GO_HOME;
        }

    }
	
	/**
     * Handle course module deletion
     */
    public static Result delete(String id) {
    	CourseModule.find.ref(id).delete();
        flash("success", "Course module has been deleted");
        return GO_HOME;
    }
}
