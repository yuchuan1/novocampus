import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import models.Group;
import models.SchoolYear;
import models.SecurityRole;
import models.Subject;
import models.User;
import models.enums.Gender;
import models.enums.GradeLevel;
import models.enums.Semester;

import com.feth.play.module.pa.PlayAuthenticate;
import com.feth.play.module.pa.PlayAuthenticate.Resolver;
import com.feth.play.module.pa.exceptions.AccessDeniedException;
import com.feth.play.module.pa.exceptions.AuthException;

import controllers.UserController;
import controllers.routes;
import play.Application;
import play.GlobalSettings;
import play.Play;
import play.mvc.Call;
import providers.MyUsernamePasswordAuthUser;
import providers.MyUsernamePasswordAuthProvider.MySignup;
import service.MyUserServicePlugin;

public class Global extends GlobalSettings {

  public void onStart(Application app) {
    PlayAuthenticate.setResolver(new Resolver() {

      @Override
      public Call login() {
        // Your login page
        return routes.Application.login();
      }

      @Override
      public Call afterAuth() {
        // The user will be redirected to this page after authentication
        // if no original URL was saved
        return routes.Application.index();
      }

      @Override
      public Call afterLogout() {
        return routes.Application.index();
      }

      @Override
      public Call auth(final String provider) {
        // You can provide your own authentication implementation,
        // however the default should be sufficient for most cases
        return com.feth.play.module.pa.controllers.routes.Authenticate.authenticate(provider);
      }

      @Override
      public Call askMerge() {
        return routes.Account.askMerge();
      }

      @Override
      public Call askLink() {
        return routes.Account.askLink();
      }

      @Override
      public Call onException(final AuthException e) {
        if (e instanceof AccessDeniedException) {
          return routes.Signup.oAuthDenied(((AccessDeniedException) e).getProviderKey());
        }

        // more custom problem handling here...
        return super.onException(e);
      }
    });

    initialData();
  }

  private void initialData() {

    if (SecurityRole.find.where().eq("roleName", controllers.Application.USER_ROLE).findRowCount() == 0) {
      final SecurityRole role = new SecurityRole();
      role.roleName = controllers.Application.USER_ROLE;
      role.save();
    }

    if (SecurityRole.find.where().eq("roleName", controllers.Application.ADMIN_ROLE).findRowCount() == 0) {
      final SecurityRole role = new SecurityRole();
      role.roleName = controllers.Application.ADMIN_ROLE;
      role.save();
    }

    if (SecurityRole.find.where().eq("roleName", controllers.Application.TEACHER_ROLE).findRowCount() == 0) {
      final SecurityRole role = new SecurityRole();
      role.roleName = controllers.Application.TEACHER_ROLE;
      role.save();
    }

    if (SecurityRole.find.where().eq("roleName", controllers.Application.STUDENT_ROLE).findRowCount() == 0) {
      final SecurityRole role = new SecurityRole();
      role.roleName = controllers.Application.STUDENT_ROLE;
      role.save();
    }

    if (SecurityRole.find.where().eq("roleName", controllers.Application.PARENT_ROLE).findRowCount() == 0) {
      final SecurityRole role = new SecurityRole();
      role.roleName = controllers.Application.PARENT_ROLE;
      role.save();
    }

    if (Subject.find.where().eq("name", "Art").findRowCount() == 0) {
      final Subject subject = new Subject();
      subject.name = "Art";
      subject.save();
    }

    if (Subject.find.where().eq("name", "Biology").findRowCount() == 0) {
      final Subject subject = new Subject();
      subject.name = "Biology";
      subject.save();
    }

    if (Subject.find.where().eq("name", "Culture").findRowCount() == 0) {
      final Subject subject = new Subject();
      subject.name = "Culture";
      subject.save();
    }

    if (Subject.find.where().eq("name", "History").findRowCount() == 0) {
      final Subject subject = new Subject();
      subject.name = "History";
      subject.save();
    }

    if (Subject.find.where().eq("name", "Science").findRowCount() == 0) {
      final Subject subject = new Subject();
      subject.name = "Science";
      subject.save();
    }

    if (Subject.find.where().eq("name", "Social Science").findRowCount() == 0) {
      final Subject subject = new Subject();
      subject.name = "Social Science";
      subject.save();
    }

    // create user "admin"
    if (User.find.where().eq("loginID", "admin").findRowCount() == 0) {
      MySignup signup = new MySignup();
      signup.loginID = Play.application().configuration().getString("novocampus.admin.loginID");
      signup.password = Play.application().configuration().getString("novocampus.admin.password");
      signup.repeatPassword = Play.application().configuration().getString("novocampus.admin.password");
      signup.email = Play.application().configuration().getString("novocampus.admin.email");
      MyUserServicePlugin userService = new MyUserServicePlugin(Play.application());
      MyUsernamePasswordAuthUser newUser = new MyUsernamePasswordAuthUser(signup);
      userService.save(newUser);

      User user = User.findByEmail(signup.email);
      user.firstName = Play.application().configuration().getString("novocampus.admin.firstname");
      user.lastName = Play.application().configuration().getString("novocampus.admin.lastname");
      user.gender = Gender.Female;
      user.emailValidated = true;
      user.roles = new ArrayList<>();
      user.roles.add(SecurityRole.findByRoleName(controllers.Application.USER_ROLE));
      user.roles.add(SecurityRole.findByRoleName(controllers.Application.ADMIN_ROLE));
      user.update();
    }

    // create teachers and students
    if (User.find.findRowCount() <= 1) {
      UserController.createTeacher("DavidLetterman@novocampus.com.tw", "david", "password", "David", "Letterman", Gender.Male);
      UserController.createTeacher("JeffScott@novocampus.com.tw", "Jeff", "password", "Jeff", "Scott", Gender.Male);
      UserController.createTeacher("StephanieFisher@novocampus.com.tw", "Stephanie", "password", "Stephanie", "Fisher", Gender.Female);
      UserController.createTeacher("KalaSunil@novocampus.com.tw", "kala", "password", "Kala", "Sunil", Gender.Female);
      UserController.createTeacher("ClarksonPatricia@novocampus.com.tw", "clarkson", "password", "Patricia", "Clarkson", Gender.Female);
      UserController.createStudent("JackyChan@novocampus.com.tw", "jacky", "password", "Jacky", "Chan", Gender.Male);
      UserController.createStudent("JefferyAaron@novocampus.com.tw", "JefferyAaron", "password", "Jeffery", "Aaron", Gender.Male);
      UserController.createStudent("jerry@novocampus.com.tw", "jerry", "password", "Jerry", "Aaron", Gender.Male);
    }

    // create school year data.
    try {
      if (SchoolYear.find.findRowCount() == 0) {
        new SchoolYear("102", Semester.One, "2013-08-01", "2014-01-31", "").save();
        new SchoolYear("102", Semester.Two, "2014-02-01", "2014-07-31", "").save();
        new SchoolYear("103", Semester.One, "2014-08-01", "2015-01-31", "").save();
        new SchoolYear("103", Semester.Two, "2015-02-01", "2015-07-31", "").save();
      }
    } catch (ParseException e) {
      System.out.println("Date format error.");
    }

    // create groups data.
    if (Group.find.findRowCount() == 0) {
      List<SchoolYear> semesters = SchoolYear.find.all();
      List<User> teachers = User.find.findList();
      GradeLevel[] levels = models.enums.GradeLevel.values();
      HashMap<Integer, String> map = new HashMap<Integer, String>();
      map.put(10, "(高)一");
      map.put(11, "(高)二");
      map.put(12, "(高)三");
      String[] str = {"忠","孝","仁","愛","信","義","和","平"};
      
      for (int i = 10; i <= 12; i++ ){
        for (int j = 0; j < str.length; j++ ){
          for (int k = 0; k < semesters.size(); k++ ){
            new Group(map.get(i) + "年" + str[j] + "班", map.get(i) + str[j], levels[i], semesters.get(k), teachers.get(j)).save();
          }
        }
      }

    }
    
    /*
     * if (SecurityRole.find.findRowCount() == 0) { for (final String roleName :
     * Arrays .asList(controllers.Application.USER_ROLE)) { final SecurityRole
     * role = new SecurityRole(); role.roleName = roleName; role.save(); } }
     */
  }
}