/*
	jQuery Colorbox language configuration
	language: Chinese Traditional (zh-TW)
	translated by: Molly Huang
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "當前圖檔 {current} 總共 {total}",
	previous: "上一頁",
	next: "下一頁",
	close: "關閉",
	xhrError: "無法載入此內容",
	imgError: "無法載入此圖片",
	slideshowStart: "開始播放幻燈片",
	slideshowStop: "停止播放幻燈片"
});