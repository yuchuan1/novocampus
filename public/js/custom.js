$(function() {
  
  // Date picker
  $(".useDatepicker").datepicker({
    dateFormat: "yy-mm-dd",
    changeMonth: true,
    changeYear: true,
    showButtonPanel: true,
  });
  
  var checkallClick = function() {
    var form = $(this).parents("form");  // limited in the same form
    var elements = $("input:checkbox[name='"+ $(this).attr("data") +"']", $(form));
    if($(this).prop("checked")) {
      // check all
      $(elements).each(function() {
        $(this).prop("checked", true);
      });
    } else {
      // uncheck all
      $(elements).each(function() {
        $(this).prop("checked", false);
      });           
    }
  }
  
  // when click check all checkbox
  $(".checkall").click(checkallClick);
  
});